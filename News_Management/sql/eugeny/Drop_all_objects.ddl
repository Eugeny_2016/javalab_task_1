DROP table "EUGENY"."USERS";
DROP table "EUGENY"."ROLE";
DROP table "EUGENY"."NEWS_AUTHOR";
DROP table "EUGENY"."NEWS_TAG";
DROP table "EUGENY"."COMMENTS";
DROP table "EUGENY"."AUTHOR";
DROP table "EUGENY"."TAG";
DROP table "EUGENY"."NEWS";

DROP trigger "EUGENY"."AUTHOR_TRG";
DROP trigger "EUGENY"."COMMENTS_TRG";
DROP trigger "EUGENY"."NEWS_TRG";
DROP trigger "EUGENY"."ROLE_TRG";
DROP trigger "EUGENY"."TAG_TRG";
DROP trigger "EUGENY"."USER_TRG";

DROP sequence "EUGENY"."AUTHOR_SEQ";
DROP sequence "EUGENY"."COMMENTS_SEQ";
DROP sequence "EUGENY"."NEWS_SEQ";
DROP sequence "EUGENY"."ROLE_SEQ";
DROP sequence "EUGENY"."TAG_SEQ";
DROP sequence "EUGENY"."USERS_SEQ";