package by.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.NewsDAO;
import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.SearchCriteria;

// TODO: Auto-generated Javadoc
/**
 * The Class NewsDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_test.xml"})
@SuppressWarnings("deprecation")
public class NewsDAOTest extends AbstractDAOTest {
	
	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(NewsDAOTest.class);
	
	/** The Constant NEWS_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String NEWS_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_table_data.xml";
	
	/** The Constant AUTHOR_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String AUTHOR_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/author_table_data.xml";
	
	/** The Constant NEWS_AUTHOR_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String NEWS_AUTHOR_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_author_table_data.xml";
	
	/** The Constant TAG_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String TAG_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/tag_table_data.xml";
	
	/** The Constant NEWS_TAG_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String NEWS_TAG_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_tag_table_data.xml";
	
	/** The news dao. */
	@Autowired
	private NewsDAO newsDAO;

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#configureDB()
	 */
	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#loadDataSetToInsert()
	 */
	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = { 
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TABLE_DATA_XML_FILE_LOCATION)), 
				new FlatXmlDataSetBuilder().build(new FileInputStream(AUTHOR_TABLE_DATA_XML_FILE_LOCATION)), 
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_AUTHOR_TABLE_DATA_XML_FILE_LOCATION)), 
				new FlatXmlDataSetBuilder().build(new FileInputStream(TAG_TABLE_DATA_XML_FILE_LOCATION)), 
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TAG_TABLE_DATA_XML_FILE_LOCATION))
				
		};
		return new CompositeDataSet(dataSet);
	}
	
	/**
	 * Insert news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertNews() throws DAOException {
		News news = new News("Title4", "Short text4", "Full text4", Timestamp.valueOf("2016-04-20 12:10:30.12"), new Date(System.currentTimeMillis()));
		long id = newsDAO.insert(news);
		Assert.assertNotEquals(0L, id);
	}
	
	/**
	 * Select news by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectNewsById() throws DAOException {
		Long newsId = new Long(1);
		News newsExpected = new News(newsId, "Title1", "Short text1","Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), new Date(116, 03, 20));
		News newsRecieved = newsDAO.selectById(newsId);
		Assert.assertEquals(newsExpected, newsRecieved);
	}
	
	/**
	 * Select all news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAllNews() throws DAOException {
		List<News> listOfNews = null;
		listOfNews = (List<News>) newsDAO.selectAll();
		Assert.assertEquals(3, listOfNews.size());
	}
	
	/**
	 * Update news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void updateNews() throws DAOException {
		Long newsId = new Long(1);
		News newsToUpdate = new News(newsId, "Title1111", "Short text1","Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), new Date(116, 03, 20));
		newsDAO.update(newsToUpdate);
		News newsRecieved = newsDAO.selectById(newsId);
		Assert.assertEquals(newsToUpdate, newsRecieved);
	}
	
	/**
	 * Delete news by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test(expected = DAOException.class)
	public void deleteNewsById() throws DAOException {
		Long newsId = new Long(1);
		newsDAO.deleteById(newsId);
		/*News newsRecieved = newsDAO.selectById(newsId);
		Assert.assertEquals(newsRecieved, new News(null, null, null, null, null, null));*/
	}
	
	/**
	 * Select news by title.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectNewsByTitle() throws DAOException {
		String newsTitle = "Title1";
		Long newsId = new Long(1);
		News newsExpected = new News(newsId, "Title1", "Short text1","Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), new Date(116, 03, 20));
		News newsRecieved = newsDAO.selectByTitle(newsTitle);
		Assert.assertEquals(newsExpected, newsRecieved);
	}
	
	/**
	 * Select several news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectSeveralNews() throws DAOException {
		List<Long> newsIds = new ArrayList<Long>();
		Long newsId1 = new Long(2);
		Long newsId2 = new Long(3);
		newsIds.add(newsId1); newsIds.add(newsId2);
		
		List<News> newsExpected = new ArrayList<News>();
		News news2 = new News(newsId1, "Title2", "Short text2","Full text2", Timestamp.valueOf("2016-04-20 15:10:30.10"), new Date(116, 03, 20));
		News news3 = new News(newsId2, "Title3", "Short text3","Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), new Date(116, 03, 20));
		newsExpected.add(news2); newsExpected.add(news3);
		
		List<News> newsRecieved = (List<News>) newsDAO.selectSeveralNews(newsIds);
		Assert.assertEquals(newsExpected, newsRecieved);
	}
	
	/**
	 * Select next n_ news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectNextN_News() throws DAOException {
		Long newsId1 = new Long(2);
		Long newsId2 = new Long(3);
		News news2 = new News(newsId1, "Title2", "Short text2","Full text2", Timestamp.valueOf("2016-04-20 15:10:30.10"), new Date(116, 03, 20));
		News news3 = new News(newsId2, "Title3", "Short text3","Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), new Date(116, 03, 20));
		
		List<News> newsExpected = new ArrayList<News>();
		newsExpected.add(news3); newsExpected.add(news2);
		
		List<News> newsRecieved = (List<News>) newsDAO.selectNextN_News(3, 1);
		Assert.assertEquals(newsExpected, newsRecieved);
	}
	
	/**
	 * Select by search criteria.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectBySearchCriteria() throws DAOException {
		List<Long> listOfAuthorIds = new ArrayList<Long>();
		List<Long> listOfTagIds = new ArrayList<Long>();
		
		listOfAuthorIds.add(new Long(2));
		listOfAuthorIds.add(new Long(3));
		listOfTagIds.add(new Long(3));
		listOfTagIds.add(new Long(4));
		SearchCriteria src = new SearchCriteria(listOfAuthorIds, listOfTagIds);
		
		Long newsId = new Long(3);
		News news = new News(newsId, "Title3", "Short text3","Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), new Date(116, 03, 20));
		
		List<News> newsExpected = new ArrayList<News>();
		newsExpected.add(news);
		
		List<News> newsRecieved = (List<News>) newsDAO.selectBySearchCriteria(src);
		Assert.assertEquals(newsExpected, newsRecieved);
	}
	
	/**
	 * Delete by title.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test(expected = DAOException.class)
	public void deleteByTitle() throws DAOException {
		String newsTitle = "Title1";
		newsDAO.deleteByTitle(newsTitle);
		/*News newsRecieved = newsDAO.selectById(new Long(1));
		Assert.assertEquals(null, newsRecieved);*/
	}
	
	/**
	 * Select amount of news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAmountOfNews() throws DAOException {
		long amountOfNewsExpected = 3L;
		long amountOfNewsRecieved = newsDAO.selectAmountOfNews();
		Assert.assertEquals(amountOfNewsExpected, amountOfNewsRecieved);
	}
	
	/**
	 * Select amount of news by search criteria.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAmountOfNewsBySearchCriteria() throws DAOException {
		List<Long> listOfAuthorIds = new ArrayList<Long>();
		List<Long> listOfTagIds = new ArrayList<Long>();
		
		listOfAuthorIds.add(new Long(2));
		listOfAuthorIds.add(new Long(3));
		listOfTagIds.add(new Long(3));
		listOfTagIds.add(new Long(4));
		SearchCriteria src = new SearchCriteria(listOfAuthorIds, listOfTagIds);
		
		long amountOfNewsExpected = 1;
		long amountOfNewsRecieved = newsDAO.selectAmountOfNewsBySearchCriteria(src);
		Assert.assertEquals(amountOfNewsExpected, amountOfNewsRecieved);
	}
}
