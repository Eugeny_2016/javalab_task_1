package by.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.RoleDAO;
import by.epam.newsmng.domain.Role;

// TODO: Auto-generated Javadoc
/**
 * The Class RoleDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_test.xml"})
public class RoleDAOTest extends AbstractDAOTest {
	
	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(RoleDAOTest.class);
	
	/** The Constant ROLE_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String ROLE_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/role_table_data.xml";
	
	/** The role dao. */
	@Autowired
	private RoleDAO roleDAO;

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#configureDB()
	 */
	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#loadDataSetToInsert()
	 */
	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = { 
				new FlatXmlDataSetBuilder().build(new FileInputStream(ROLE_TABLE_DATA_XML_FILE_LOCATION))
		};
		return new CompositeDataSet(dataSet);
	}
	
	/**
	 * Insert tag.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertTag() throws DAOException {
		Role role = new Role("newRole");
		long id = roleDAO.insert(role);
		Assert.assertEquals(3L, id);
	}
	
	/**
	 * Select tag by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectTagById() throws DAOException {
		Role roleExpected = new Role(1L, "administrator");
		Role roleRecieved = roleDAO.selectById(new Long(1));
		Assert.assertEquals(roleExpected, roleRecieved);
	}
	
	/**
	 * Select all tags.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAllTags() throws DAOException {
		List<Role> listOfRoles = null;
		listOfRoles = (List<Role>) roleDAO.selectAll();
		Assert.assertEquals(2, listOfRoles.size());
	}
	
	/**
	 * Update tag.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void updateTag() throws DAOException {
		Role roleToUpdate = new Role(1L, "adminus");
		roleDAO.update(roleToUpdate);
		Role roleRecieved = roleDAO.selectById(roleToUpdate.getRoleId());
		Assert.assertEquals(roleToUpdate, roleRecieved);
	}
	
	/**
	 * Delete tag by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test()
	public void deleteTagById() throws DAOException {
		Long roleId = new Long(1);
		roleDAO.deleteById(roleId);
		Role roleRecieved = roleDAO.selectById(roleId);
		Assert.assertEquals(roleRecieved, new Role(null, null));
	}
	
	/**
	 * Select by role.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectByRole() throws DAOException {
		Role roleExpected = new Role(1L, "administrator");
		Role roleRecieved = roleDAO.selectByName("administrator");
		Assert.assertEquals(roleExpected, roleRecieved);
	}
}
