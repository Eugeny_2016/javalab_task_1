package by.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.UserDAO;
import by.epam.newsmng.domain.User;

// TODO: Auto-generated Javadoc
/**
 * The Class UserDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_test.xml"})
public class UserDAOTest extends AbstractDAOTest {
	
	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(UserDAOTest.class);
	
	/** The Constant ROLE_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String ROLE_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/role_table_data.xml";
	
	/** The Constant USER_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String USER_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/user_table_data.xml";
	
	/** The user dao. */
	@Autowired
	private UserDAO userDAO;

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#configureDB()
	 */
	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#loadDataSetToInsert()
	 */
	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = { 
				new FlatXmlDataSetBuilder().build(new FileInputStream(ROLE_TABLE_DATA_XML_FILE_LOCATION)), 
				new FlatXmlDataSetBuilder().build(new FileInputStream(USER_TABLE_DATA_XML_FILE_LOCATION))
		};
		return new CompositeDataSet(dataSet);
	}
	
	/**
	 * Insert user.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertUser() throws DAOException {
		User user = new User(2L, "Vasya", "user3", "000");
		long id = userDAO.insert(user);
		Assert.assertEquals(4L, id);
	}
	
	/**
	 * Select user by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectUserById() throws DAOException {
		User userExpected = new User(1L, 1L, "Eugeny", "admin", "123");
		User userRecieved = userDAO.selectById(new Long(1));
		Assert.assertEquals(userExpected, userRecieved);
	}
	
	/**
	 * Select all users.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAllUsers() throws DAOException {
		List<User> listOfUsers = null;
		listOfUsers = (List<User>) userDAO.selectAll();
		Assert.assertEquals(3, listOfUsers.size());
	}
	
	/**
	 * Update user.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void updateUser() throws DAOException {
		User userToUpdate = new User(1L, 1L, "Eugeny", "adminus", "123");
		userDAO.update(userToUpdate);
		User userRecieved = userDAO.selectById(userToUpdate.getUserId());
		Assert.assertEquals(userToUpdate, userRecieved);
	}
	
	/**
	 * Delete user by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test()
	public void deleteUserById() throws DAOException {
		Long userId = new Long(1);
		userDAO.deleteById(userId);
		User userRecieved = userDAO.selectById(userId);
		Assert.assertEquals(userRecieved, new User(null, null, null, null, null));
	}
	
	/**
	 * Select user by login.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectUserByLogin() throws DAOException {
		User userExpected = new User(1L, 1L, "Eugeny", "admin", "123");
		User userRecieved = userDAO.selectByLogin("admin");
		Assert.assertEquals(userExpected, userRecieved);
	}
}
