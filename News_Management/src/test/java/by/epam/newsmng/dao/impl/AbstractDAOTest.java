package by.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractDAOTest.
 */
public abstract class AbstractDAOTest {
	
	/** The Constant DELETE_ALL_DATA_XML_FILE_LOCATION. */
	private static final String DELETE_ALL_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/delete_data.xml";
	
	/** The Constant SEQUENCE_RESETTER_ORACLE. */
	public static final DatabaseOperation SEQUENCE_RESETTER_ORACLE = new DatabaseOperation() {
	    @Override
	    public void execute(IDatabaseConnection connection, IDataSet dataSet) throws DatabaseUnitException, SQLException {
	        String[] tables = dataSet.getTableNames();
	        Statement statement = connection.getConnection().createStatement();
	        for (String table : tables) {
	        	if (!table.contains("_")) {	        		
		        	int startWith = dataSet.getTable(table).getRowCount() + 1;
		        	statement.execute("drop sequence " + table + "_SEQ");
		            statement.execute("create sequence " + table + "_SEQ START WITH " + startWith);
	        	}
	        }
	    }
	};
	
	/** The data source. */
	@Autowired
	DataSource dataSource;
	
	/** The con. */
	Connection con;
	
	/**
	 * Instantiates a new abstract dao test.
	 */
	public AbstractDAOTest() {
		Locale.setDefault(Locale.ENGLISH);
	}
	
	/**
	 * Take connection.
	 *
	 * @return the i database connection
	 * @throws SQLException the SQL exception
	 * @throws DatabaseUnitException the database unit exception
	 */
	public IDatabaseConnection takeConnection() throws SQLException, DatabaseUnitException {
		con = dataSource.getConnection();
		DatabaseMetaData dbMeta = con.getMetaData();
		IDatabaseConnection connection = new DatabaseConnection(con, dbMeta.getUserName());
		connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
		return connection;
	}
	
	/**
	 * Configure db.
	 *
	 * @throws DatabaseUnitException the database unit exception
	 * @throws SQLException the SQL exception
	 * @throws FileNotFoundException the file not found exception
	 */
	public abstract void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException;
	
	/**
	 * Load data set to insert.
	 *
	 * @return the i data set
	 * @throws FileNotFoundException the file not found exception
	 * @throws DataSetException the data set exception
	 */
	public abstract IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException;
	
	/**
	 * Load data set to delete.
	 *
	 * @return the i data set
	 * @throws FileNotFoundException the file not found exception
	 * @throws DataSetException the data set exception
	 */
	public IDataSet loadDataSetToDelete() throws FileNotFoundException, DataSetException {
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(DELETE_ALL_DATA_XML_FILE_LOCATION));
		return dataSet;
	}
	
	/**
	 * Clean data base.
	 *
	 * @throws DatabaseUnitException the database unit exception
	 * @throws SQLException the SQL exception
	 * @throws FileNotFoundException the file not found exception
	 */
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToDelete());
		releaseConnection();
	}
	
	/**
	 * Release connection.
	 *
	 * @throws SQLException the SQL exception
	 */
	@After
	public void releaseConnection() throws SQLException {
		con.close();
	}	
}