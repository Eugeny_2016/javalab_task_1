package by.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsmng.dao.CommentDAO;
import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.domain.Comment;

// TODO: Auto-generated Javadoc
/**
 * The Class CommentDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_test.xml"})
public class CommentDAOTest extends AbstractDAOTest {
	
	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(CommentDAOTest.class);
	
	/** The Constant NEWS_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String NEWS_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_table_data.xml";
	
	/** The Constant COMMENTS_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String COMMENTS_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/comment_table_data.xml";
	
	/** The comment dao. */
	@Autowired
	private CommentDAO commentDAO;

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#configureDB()
	 */
	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#loadDataSetToInsert()
	 */
	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = { 
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(COMMENTS_TABLE_DATA_XML_FILE_LOCATION))
		};
		return new CompositeDataSet(dataSet);
	}
	
	/**
	 * Insert comment.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertComment() throws DAOException {
		Comment comment = new Comment(new Long(3), "Third", Timestamp.valueOf("2016-03-20 12:10:30.12"));
		long id = commentDAO.insert(comment);
		Assert.assertEquals(4L, id);
	}
	
	/**
	 * Select comment by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectCommentById() throws DAOException {
		Long commentId = new Long(1);
		Comment commentExpected = new Comment(commentId, new Long(1), "Text1", Timestamp.valueOf("2016-04-20 10:10:30.12"));
		Comment commentRecieved = commentDAO.selectById(commentId);
		Assert.assertEquals(commentExpected, commentRecieved);
	}
	
	/**
	 * Select all comments.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAllComments() throws DAOException {
		List<Comment> listOfComments = null;
		listOfComments = (List<Comment>) commentDAO.selectAll();
		Assert.assertEquals(3, listOfComments.size());
	}
	
	/**
	 * Update comment.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void updateComment() throws DAOException {
		Comment commentToUpdate = new Comment(new Long(1), new Long(1), "Text111111", Timestamp.valueOf("2016-04-20 10:10:30.12"));
		commentDAO.update(commentToUpdate);
		Comment commentRecieved = commentDAO.selectById(commentToUpdate.getCommentId());
		Assert.assertEquals(commentToUpdate, commentRecieved);
	}
	
	/**
	 * Delete comment by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteCommentById() throws DAOException {
		Long commentId = new Long(1);
		commentDAO.deleteById(commentId);
		/*Comment commentRecieved = commentDAO.selectById(commentId);
		Assert.assertEquals(commentRecieved, new Comment(null, null, null, null));*/
	}
	
	/**
	 * Select all comments by news id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAllCommentsByNewsId() throws DAOException {
		Long newsId = new Long(2);
		List<Comment> listOfCommentsExpected = new ArrayList<Comment>();
		
		Comment com1 = new Comment(new Long(2), newsId, "Text2", Timestamp.valueOf("2016-04-20 10:15:30.19"));
		Comment com2 = new Comment(new Long(3), newsId, "Text3", Timestamp.valueOf("2016-04-20 15:10:30.11"));
		listOfCommentsExpected.add(com1); listOfCommentsExpected.add(com2);
		
		List<Comment> listOfCommentsRecieved = (List<Comment>) commentDAO.selectAllCommentsByNewsId(newsId);
		Assert.assertEquals(listOfCommentsExpected, listOfCommentsRecieved);
	}
	
	/**
	 * Update comment with censuring.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void updateCommentWithCensuring() throws DAOException {
		commentDAO.updateCommentWithCensuring(new Long(1), "Some reason");
		String newString = "НЛО прилетело и удалило эту запись! Some reason";
		
		Comment commentExpected = new Comment(new Long(1), new Long(1), newString, Timestamp.valueOf("2016-04-20 10:10:30.12"));
		Comment commentRecieved = commentDAO.selectById(new Long(1));
		
		Assert.assertEquals(commentExpected, commentRecieved);
	}
	
	
	/**
	 * Delete comment from news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteCommentFromNews() throws DAOException {
		Long commentId = new Long(1);
		Long newsId = new Long(1);
		commentDAO.deleteCommentFromNews(newsId, commentId);
		Comment commentRecieved = commentDAO.selectById(commentId);
		Assert.assertEquals(commentRecieved.getCommentId(), null);
	}
	
	/**
	 * Delete all comments from news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteAllCommentsFromNews() throws DAOException {
		Long newsId = new Long(1);
		commentDAO.deleteAllCommentsFromNews(newsId);
		Collection<Comment> commentsRecieved = commentDAO.selectAllCommentsByNewsId(newsId);
		Assert.assertEquals(commentsRecieved, new ArrayList<Comment>());
	}
	
	/**
	 * Select comment by news id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectCommentByNewsId() throws DAOException {
		List<Long> listOfNewsIdExpected = new ArrayList<Long>();
		List<Long> listOfNewsIdRecieved = new ArrayList<Long>();
		
		listOfNewsIdExpected.add(new Long(2));
		listOfNewsIdExpected.add(new Long(1));
		listOfNewsIdRecieved = (List<Long>) commentDAO.selectNMostCommentedNewsId(2);
		Assert.assertEquals(listOfNewsIdExpected, listOfNewsIdRecieved);
	}
	
	/**
	 * Select comments by each news id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectCommentsByEachNewsId() throws DAOException {
		Map<Long, Collection<Comment>> mapOfNewsIdsAndCommentsExpected = new LinkedHashMap<Long, Collection<Comment>>();
		Map<Long, Collection<Comment>> mapOfNewsIdsAndCommentsRecieved = null;
		
		Long newsId1 = new Long(1);
		Long newsId2 = new Long(2);
		Collection<Long> newsIds = new ArrayList<Long>();
		newsIds.add(newsId1); newsIds.add(newsId2);
		
		Comment comment1 = new Comment(new Long(1), new Long(1), "Text1", Timestamp.valueOf("2016-04-20 10:10:30.12"));
		Comment comment2 = new Comment(new Long(2), new Long(2), "Text2", Timestamp.valueOf("2016-04-20 10:15:30.19"));
		Comment comment3 = new Comment(new Long(3), new Long(2), "Text3", Timestamp.valueOf("2016-04-20 15:10:30.11"));
		
		List<Comment> authorCollection1 = new ArrayList<Comment>();
		List<Comment> authorCollection2 = new ArrayList<Comment>();
		
		authorCollection1.add(comment1);
		authorCollection2.add(comment2); authorCollection2.add(comment3);
		
		mapOfNewsIdsAndCommentsExpected.put(newsId1, authorCollection1);
		mapOfNewsIdsAndCommentsExpected.put(newsId2, authorCollection2);
		
		mapOfNewsIdsAndCommentsRecieved = commentDAO.selectCommentsByEachNewsId(newsIds);
		Assert.assertEquals(mapOfNewsIdsAndCommentsExpected, mapOfNewsIdsAndCommentsRecieved);
	}
}
