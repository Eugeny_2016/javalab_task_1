package by.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsmng.dao.AuthorDAO;
import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.domain.Author;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthorDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_test.xml"})
public class AuthorDAOTest extends AbstractDAOTest {
	
	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(AuthorDAOTest.class);
	
	/** The Constant AUTHOR_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String AUTHOR_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/author_table_data.xml";
	
	/** The Constant NEWS_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String NEWS_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_table_data.xml";
	
	/** The Constant NEWS_AUTHOR_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String NEWS_AUTHOR_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_author_table_data.xml";
	
	/** The author dao. */
	@Autowired
	private AuthorDAO authorDAO;

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#configureDB()
	 */
	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#loadDataSetToInsert()
	 */
	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = {
				new FlatXmlDataSetBuilder().build(new FileInputStream(AUTHOR_TABLE_DATA_XML_FILE_LOCATION)), 
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_AUTHOR_TABLE_DATA_XML_FILE_LOCATION))
		};
		return new CompositeDataSet(dataSet);
	}
	
	/**
	 * Insert author.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertAuthor() throws DAOException {
		Author author = new Author();
		author.setAuthorName("Petrov");
		long id = authorDAO.insert(author);
		Assert.assertEquals(4L, id);
	}
	
	/**
	 * Select author by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAuthorById() throws DAOException {
		Long authorId = new Long(1);
		Author authorExpected = new Author(authorId, "Pushkin", null);
		Author authorRecieved = authorDAO.selectById(authorId);
		Assert.assertEquals(authorExpected, authorRecieved);
	}
	
	/**
	 * Select all authors.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAllAuthors() throws DAOException {
		List<Author> listOfAuthors = null;
		listOfAuthors = (List<Author>) authorDAO.selectAll();
		Assert.assertEquals(3, listOfAuthors.size());
	}
	
	/**
	 * Update author.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void updateAuthor() throws DAOException {
		Author authorToUpdate = new Author(new Long(1), "Aleksander Pushkin", null);
		authorDAO.update(authorToUpdate);
		Author authorRecieved = authorDAO.selectById(authorToUpdate.getAuthorId());
		Assert.assertEquals(authorToUpdate, authorRecieved);
	}
	
	/**
	 * Delete author by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test(expected = DAOException.class)
	public void deleteAuthorById() throws DAOException {
		Long authorId = new Long(1);
		authorDAO.deleteById(authorId);
		/*Author authorRecieved = authorDAO.selectById(authorId);
		Assert.assertNotEquals(authorRecieved, new Author(null, null, null));*/
	}
	
	/**
	 * Make author expired.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void makeAuthorExpired() throws DAOException {
		Timestamp tstmp = new Timestamp(System.currentTimeMillis());
		Author authorExpected = new Author(new Long(1), "Pushkin", tstmp);
		
		authorDAO.updateAuthorExpiredById(new Long(1), tstmp);
		Author authorRecieved = authorDAO.selectById(authorExpected.getAuthorId());
		Assert.assertEquals(authorExpected, authorRecieved);
	}
	
	/**
	 * Select author by name.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAuthorByName() throws DAOException {
		String authorName = "Pushkin";
		Author authorExpected = new Author(new Long(1), "Pushkin", null);
		Author authorRecieved = authorDAO.selectByName(authorName);
		Assert.assertEquals(authorExpected, authorRecieved);
	}
	
	/**
	 * Select existing authors by names.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectExistingAuthorsByNames() throws DAOException {
		Author author1 = new Author(new Long(1), "Pushkin", null);
		Author author2 = new Author(new Long(3), "Gogol", null);
		List<Author> collectionExpected = new ArrayList<Author>();
		collectionExpected.add(author1);
		collectionExpected.add(author2);
		List<String> authorNames = new ArrayList<String>();
		authorNames.add("Pushkin");
		authorNames.add("Gogol");
		List<Author> collectionRecieved = (List<Author>) authorDAO.selectExistingAuthorsByNames(authorNames);
		Assert.assertEquals(collectionExpected, collectionRecieved);
	}
	
	/**
	 * Insert all authors.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertAllAuthors() throws DAOException {
		Author author1 = new Author("Ivanov", null);
		Author author2 = new Author("Petrov", null);
		Author author3 = new Author("Sidorov", null);
		List<Author> authors = new ArrayList<Author>();
		authors.add(author1); authors.add(author2); authors.add(author3);
		
		authorDAO.insertAllAuthors(authors);
		author1.setAuthorId(new Long(4));
		author2.setAuthorId(new Long(5));
		author3.setAuthorId(new Long(6));
		
		List<String> authorsName = new ArrayList<String>();
		authorsName.add("Ivanov");
		authorsName.add("Petrov");
		authorsName.add("Sidorov");
		List<Author> authorsRecieved = (List<Author>) authorDAO.selectExistingAuthorsByNames(authorsName);
		
		Assert.assertEquals(authorsRecieved, authors);
	}
	
	/**
	 * Insert authors to news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertAuthorsToNews() throws DAOException {
		Author author1 = new Author("Pushkin", null);
		Author author2 = new Author("Lermontov", null);
		Author author4 = new Author("Ivanov", null);
		Author author5 = new Author("Petrov", null);
		Author author6 = new Author("Sidorov", null);
		
		List<Author> authors = new ArrayList<Author>();
		authors.add(author4); authors.add(author5); authors.add(author6);
		authorDAO.insertAllAuthors(authors);
		
		author1.setAuthorId(new Long(1));
		author2.setAuthorId(new Long(2));
		author4.setAuthorId(new Long(4));
		author5.setAuthorId(new Long(5));
		author6.setAuthorId(new Long(6));
		
		List<Long> listOfAuthorIds = new ArrayList<Long>();
		listOfAuthorIds.add(new Long(4));
		listOfAuthorIds.add(new Long(5));
		listOfAuthorIds.add(new Long(6));
		authorDAO.insertAuthorsToNews(new Long(1), listOfAuthorIds);
		
		authors.clear();
		authors.add(author1); authors.add(author2); authors.add(author4); authors.add(author5); authors.add(author6);
		List<Author> authorsRecieved = (List<Author>) authorDAO.selectAuthorsByNewsId(new Long(1));
		
		Assert.assertEquals(authors, authorsRecieved);
	}
	
	/**
	 * Select authors by news id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAuthorsByNewsId() throws DAOException {
		Author author1 = new Author(new Long(1), "Pushkin", null);
		Author author2 = new Author(new Long(2), "Lermontov", null);
		
		List<Author> authors = new ArrayList<Author>();
		authors.add(author1);
		authors.add(author2);
	
		List<Author> authorsRecieved = (List<Author>) authorDAO.selectAuthorsByNewsId(new Long(1));
		Assert.assertEquals(authors, authorsRecieved);
	}
	
	/**
	 * Select authors by each news id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAuthorsByEachNewsId() throws DAOException {
		Map<Long, Collection<Author>> mapOfNewsIdsAndAuthorsExpected = new LinkedHashMap<Long, Collection<Author>>();
		Map<Long, Collection<Author>> mapOfNewsIdsAndAuthorsRecieved = null;
		
		Long newsId1 = new Long(1);
		Long newsId2 = new Long(2);
		Long newsId3 = new Long(3);
		Collection<Long> newsIds = new ArrayList<Long>();
		newsIds.add(newsId1); newsIds.add(newsId2); newsIds.add(newsId3);
		
		Author author1 = new Author(new Long(1), "Pushkin", null);
		Author author2 = new Author(new Long(2), "Lermontov", null);
		Author author3 = new Author(new Long(3), "Gogol", null);
		
		List<Author> authorCollection1 = new ArrayList<Author>();
		List<Author> authorCollection2 = new ArrayList<Author>();
		List<Author> authorCollection3 = new ArrayList<Author>();
		
		authorCollection1.add(author1); authorCollection1.add(author2);
		authorCollection2.add(author1); authorCollection2.add(author2); authorCollection2.add(author3);
		authorCollection3.add(author2); authorCollection3.add(author3);
		
		mapOfNewsIdsAndAuthorsExpected.put(newsId1, authorCollection1);
		mapOfNewsIdsAndAuthorsExpected.put(newsId2, authorCollection2);
		mapOfNewsIdsAndAuthorsExpected.put(newsId3, authorCollection3);
		
		mapOfNewsIdsAndAuthorsRecieved = authorDAO.selectAuthorsByEachNewsId(newsIds);
		Assert.assertEquals(mapOfNewsIdsAndAuthorsExpected, mapOfNewsIdsAndAuthorsRecieved);
	}
	
	/**
	 * Delete author from news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteAuthorFromNews() throws DAOException {
		Author author = new Author(new Long(3), "Gogol", null);
		authorDAO.deleteAuthorFromNews(author.getAuthorId(), new Long(2));
		
		List<Author> authorsExpected = new ArrayList<Author>();
		authorsExpected.add(author);
	
		List<Author> authorsRecieved = (List<Author>) authorDAO.selectAuthorsByNewsId(new Long(3));
		Assert.assertEquals(authorsExpected, authorsRecieved);
	}
	
	/**
	 * Delete all authors from news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteAllAuthorsFromNews() throws DAOException {
		authorDAO.deleteAllAuthorsFromNews(new Long(3));
		
		List<Author> authorsExpected = new ArrayList<Author>();
	
		List<Author> authorsRecieved = (List<Author>) authorDAO.selectAuthorsByNewsId(new Long(3));
		Assert.assertEquals(authorsExpected, authorsRecieved);
	}

}
