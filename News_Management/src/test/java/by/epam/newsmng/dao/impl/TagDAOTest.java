package by.epam.newsmng.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.TagDAO;
import by.epam.newsmng.domain.Tag;

// TODO: Auto-generated Javadoc
/**
 * The Class TagDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_test.xml"})
public class TagDAOTest extends AbstractDAOTest {
	
	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(TagDAOTest.class);
	
	/** The Constant TAG_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String TAG_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/tag_table_data.xml";
	
	/** The Constant NEWS_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String NEWS_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_table_data.xml";
	
	/** The Constant NEWS_TAG_TABLE_DATA_XML_FILE_LOCATION. */
	private static final String NEWS_TAG_TABLE_DATA_XML_FILE_LOCATION = "src/test/resources/db_data/news_tag_table_data.xml";
	
	/** The tag dao. */
	@Autowired
	private TagDAO tagDAO;

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#configureDB()
	 */
	@Override
	@Before
	public void configureDB() throws DatabaseUnitException, SQLException, FileNotFoundException {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), loadDataSetToInsert());
		SEQUENCE_RESETTER_ORACLE.execute(takeConnection(), loadDataSetToInsert());

	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.impl.AbstractDAOTest#loadDataSetToInsert()
	 */
	@Override
	public IDataSet loadDataSetToInsert() throws FileNotFoundException, DataSetException {
		IDataSet[] dataSet = { 
				new FlatXmlDataSetBuilder().build(new FileInputStream(TAG_TABLE_DATA_XML_FILE_LOCATION)), 
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TABLE_DATA_XML_FILE_LOCATION)),
				new FlatXmlDataSetBuilder().build(new FileInputStream(NEWS_TAG_TABLE_DATA_XML_FILE_LOCATION))
		};
		return new CompositeDataSet(dataSet);
	}
	
	/**
	 * Insert tag.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertTag() throws DAOException {
		Tag tag = new Tag("Tag5");
		long id = tagDAO.insert(tag);
		Assert.assertEquals(5L, id);
	}
	
	/**
	 * Select tag by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectTagById() throws DAOException {
		Tag tagExpected = new Tag(new Long(4), "history");
		Tag tagRecieved = tagDAO.selectById(new Long(4));
		Assert.assertEquals(tagExpected, tagRecieved);
	}
	
	/**
	 * Select all tags.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAllTags() throws DAOException {
		List<Tag> listOfTags = null;
		listOfTags = (List<Tag>) tagDAO.selectAll();
		Assert.assertEquals(4, listOfTags.size());
	}
	
	/**
	 * Update tag.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void updateTag() throws DAOException {
		Tag tagToUpdate = new Tag(new Long(4), "pistory");
		tagDAO.update(tagToUpdate);
		Tag tagRecieved = tagDAO.selectById(tagToUpdate.getTagId());
		Assert.assertEquals(tagToUpdate, tagRecieved);
	}
	
	/**
	 * Delete tag by id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test(expected = DAOException.class)
	public void deleteTagById() throws DAOException {
		Long tagId = new Long(1);
		tagDAO.deleteById(tagId);
		/*Tag tagRecieved = tagDAO.selectById(tagId);
		Assert.assertEquals(tagRecieved, new Tag(null, null));*/
	}
	
	/**
	 * Select all tags by news id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectAllTagsByNewsId() throws DAOException {
		Tag tag1 = new Tag(new Long(3), "18 vek");
		Tag tag2 = new Tag(new Long(4), "history");
		
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag1);
		tags.add(tag2);
	
		List<Tag> tagsRecieved = (List<Tag>) tagDAO.selectAllTagsByNewsId(new Long(3));
		Assert.assertEquals(tags, tagsRecieved);
	}
	
	/**
	 * Select existing tags by names.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectExistingTagsByNames() throws DAOException {
		List<String> listOfTagNames = new ArrayList<String>();
		listOfTagNames.add("18 vek"); listOfTagNames.add("not existing tag"); listOfTagNames.add("history");
		
		List<Tag> listOfTagsExpected = new ArrayList<Tag>();
		Tag tag1 = new Tag(new Long(3), "18 vek");
		Tag tag2 = new Tag(new Long(4), "history");
		listOfTagsExpected.add(tag1); listOfTagsExpected.add(tag2);
		
		List<Tag> listOfTagsRecieved = (List<Tag>) tagDAO.selectExistingTagsByNames(listOfTagNames);
		Assert.assertEquals(listOfTagsExpected, listOfTagsRecieved);
	}
	
	/**
	 * Insert all tags.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertAllTags() throws DAOException {
		Tag tag1 = new Tag("Frist");
		Tag tag2 = new Tag("Second");
		Tag tag3 = new Tag("Third");
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag1); tags.add(tag2); tags.add(tag3);
		
		tagDAO.insertAllTags(tags);
		tag1.setTagId(new Long(5));
		tag2.setTagId(new Long(6));
		tag3.setTagId(new Long(7));
		
		List<String> tagsName = new ArrayList<String>();
		tagsName.add("Frist");
		tagsName.add("Second");
		tagsName.add("Third");
		List<Tag> tagsRecieved = (List<Tag>) tagDAO.selectExistingTagsByNames(tagsName);
		
		Assert.assertEquals(tagsRecieved, tags);
	}
		
	/**
	 * Insert tags to news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void insertTagsToNews() throws DAOException {
		Tag tag1 = new Tag("18 vek");
		Tag tag2 = new Tag("history");
		Tag tag3 = new Tag("First");
		Tag tag4 = new Tag("Second");
		
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(tag3); tags.add(tag4);
		tagDAO.insertAllTags(tags);
		
		tag1.setTagId(new Long(3));
		tag2.setTagId(new Long(4));
		tag3.setTagId(new Long(5));
		tag4.setTagId(new Long(6));
		
		List<Long> listOfTagIds = new ArrayList<Long>();
		listOfTagIds.add(new Long(5));
		listOfTagIds.add(new Long(6));
		tagDAO.insertTagsToNews(new Long(3), listOfTagIds);
		
		tags.clear();
		tags.add(tag1); tags.add(tag2); tags.add(tag3); tags.add(tag4);
		List<Tag> tagsRecieved = (List<Tag>) tagDAO.selectAllTagsByNewsId(new Long(3));
		
		Assert.assertEquals(tags, tagsRecieved);
	}
	
	/**
	 * Select comments by each news id.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void selectCommentsByEachNewsId() throws DAOException {
		Map<Long, Collection<Tag>> mapOfNewsIdsAndTagsExpected = new LinkedHashMap<Long, Collection<Tag>>();
		Map<Long, Collection<Tag>> mapOfNewsIdsAndTagsRecieved = null;
		
		Long newsId1 = new Long(1);
		Long newsId2 = new Long(2);
		Long newsId3 = new Long(3);
		Collection<Long> newsIds = new ArrayList<Long>();
		newsIds.add(newsId1); newsIds.add(newsId2); newsIds.add(newsId3);
		
		Tag tag1 = new Tag(new Long(1), "russian classical literature");
		Tag tag2 = new Tag(new Long(2), "mertvie dushi");
		Tag tag3 = new Tag(new Long(3), "18 vek");
		Tag tag4 = new Tag(new Long(4), "history");
		
		List<Tag> tagCollection1 = new ArrayList<Tag>();
		List<Tag> tagCollection2 = new ArrayList<Tag>();
		List<Tag> tagCollection3 = new ArrayList<Tag>();
		
		tagCollection1.add(tag1);
		tagCollection2.add(tag2);
		tagCollection3.add(tag3); tagCollection3.add(tag4);
		
		mapOfNewsIdsAndTagsExpected.put(newsId1, tagCollection1);
		mapOfNewsIdsAndTagsExpected.put(newsId2, tagCollection2);
		mapOfNewsIdsAndTagsExpected.put(newsId3, tagCollection3);
		
		mapOfNewsIdsAndTagsRecieved = tagDAO.selectTagsByEachNewsId(newsIds);
		Assert.assertEquals(mapOfNewsIdsAndTagsExpected, mapOfNewsIdsAndTagsRecieved);
	}
	
	/**
	 * Delete tag from news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteTagFromNews() throws DAOException {
		Tag tag = new Tag(new Long(3), "18 vek");
		tagDAO.deleteTagFromNews(new Long(3), new Long(4));
		
		List<Tag> tagsExpected = new ArrayList<Tag>();
		tagsExpected.add(tag);
	
		List<Tag> tagsRecieved = (List<Tag>) tagDAO.selectAllTagsByNewsId(new Long(3));
		Assert.assertEquals(tagsExpected, tagsRecieved);
	}
	
	/**
	 * Delete al tags from news.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteAlTagsFromNews() throws DAOException {
		Long newsId = new Long(3);
		tagDAO.deleteAllTagsFromNews(newsId);
		Collection<Tag> commentsRecieved = tagDAO.selectAllTagsByNewsId(newsId);
		Assert.assertEquals(commentsRecieved, new ArrayList<Tag>());
	}
}
