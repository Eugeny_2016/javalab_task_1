package by.epam.newsmng.service.impl;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.UserDAO;
import by.epam.newsmng.domain.User;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class UserServiceGroupTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceGroupTest {
	
	/** The user dao mock. */
	@Mock
	private UserDAO userDAOMock;
	
	/** The user service. */
	@InjectMocks
	private UserServiceGroup userService;
	
	/**
	 * Setup data.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Before
	public void setupData() throws DAOException {
		when(userDAOMock.insert(new User(2L, "Vasya", "user3", "000"))).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) {
				return 4L;
			}
		});
		when(userDAOMock.selectById(1L)).thenAnswer(new Answer<User>() {
			@Override
			public User answer(InvocationOnMock invocation) throws Throwable {
				User user = new User(1L, "Eugeny", "admin", "123");
				return user;
			}
		});
		when(userDAOMock.selectAll()).thenAnswer(new Answer<Collection<User>>() {
			@Override
			public Collection<User> answer(InvocationOnMock invocation) throws Throwable {
				List<User> users = new ArrayList<User>();
				User user1 = new User(1L, "Eugeny", "admin", "123");
				User user2 = new User(1L, "Macs", "user1", "456");
				User user3 = new User(1L, "Pavel", "user2", "789");
				users.add(user1); users.add(user2); users.add(user3);
				return users;
			}
		});
		when(userDAOMock.selectByLogin("admin")).thenAnswer(new Answer<User>() {
			@Override
			public User answer(InvocationOnMock invocation) throws Throwable {
				User user = new User(1L, 1L, "Eugeny", "admin", "123");
				return user;
			}
		});
	}
	
	/**
	 * Adds the.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test
    public void add() throws ServiceException {
		long recievedId = userService.add(new User(2L, "Vasya", "user3", "000"));
		Assert.assertEquals(4L, recievedId);
    }
	
	/**
	 * Gets the by id.
	 *
	 * @return the by id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getById() throws ServiceException {
		User user = new User(1L, "Eugeny", "admin", "123");
		Assert.assertEquals(user, userService.getById(1L));
    }
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAll() throws ServiceException {
		List<User> usersExpected = new ArrayList<User>();
		User user1 = new User(1L, "Eugeny", "admin", "123");
		User user2 = new User(1L, "Macs", "user1", "456");
		User user3 = new User(1L, "Pavel", "user2", "789");
		usersExpected.add(user1); usersExpected.add(user2); usersExpected.add(user3);
		Assert.assertEquals(usersExpected, userService.getAll());
    }
	
	/**
	 * Update.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void update() throws ServiceException, DAOException {
		User user = new User(1L, "Eugeny Gordienko", "admin", "123");
		userService.update(user);
		verify(userDAOMock).update(user);
	}
	
	/**
	 * Delete by id.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteById() throws ServiceException, DAOException {
		userService.deleteById(1L);
		verify(userDAOMock).deleteById(1L);
	}
	
	/**
	 * Gets the user by login.
	 *
	 * @return the user by login
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getUserByLogin() throws ServiceException {
		User user = new User(1L, 1L, "Eugeny", "admin", "123");
		Assert.assertEquals(user, userService.getByLogin("admin"));
    }
}
