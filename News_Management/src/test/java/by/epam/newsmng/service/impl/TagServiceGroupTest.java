package by.epam.newsmng.service.impl;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.TagDAO;
import by.epam.newsmng.domain.Tag;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class TagServiceGroupTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceGroupTest {
	
	/** The tag dao mock. */
	@Mock
	private TagDAO tagDAOMock;
	
	/** The tag service. */
	@InjectMocks
	private TagServiceGroup tagService;
	
	/**
	 * Setup data.
	 *
	 * @throws DAOException the DAO exception
	 */
	@SuppressWarnings("unchecked")
	@Before
	public void setupData() throws DAOException {
		when(tagDAOMock.insert(new Tag("SomeTag"))).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) {
				return 5L;
			}
		});
		when(tagDAOMock.selectById(1L)).thenAnswer(new Answer<Tag>() {
			@Override
			public Tag answer(InvocationOnMock invocation) throws Throwable {
				Tag Tag = new Tag();
				Tag.setTagName("russian classical literature");
				return Tag;
			}
		});
		when(tagDAOMock.selectAll()).thenAnswer(new Answer<Collection<Tag>>() {
			@Override
			public Collection<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> tags = new ArrayList<Tag>();
				Tag tag1 = new Tag(1L, "russian classical literature");
				Tag tag2 = new Tag(2L, "mertvie dushi");
				Tag tag3 = new Tag(3L, "18 vek");
				Tag tag4 = new Tag(4L, "history");
				tags.add(tag1); tags.add(tag2); tags.add(tag3); tags.add(tag4);
				return tags;
			}
		});
		when(tagDAOMock.selectExistingTagsByNames(any(Collection.class))).thenAnswer(new Answer<Collection<Tag>>() {
			@Override
			public Collection<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> tags = new ArrayList<Tag>();
				Tag tag1 = new Tag(1L, "russian classical literature");
				Tag tag2 = new Tag(2L, "mertvie dushi");
				tags.add(tag1); tags.add(tag2);
				return tags;
			}
		});
		when(tagDAOMock.selectAllTagsByNewsId(1L)).thenAnswer(new Answer<Collection<Tag>>() {
			@Override
			public Collection<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> tags = new ArrayList<Tag>();
				Tag tag1 = new Tag(1L, "russian classical literature");
				tags.add(tag1);
				return tags;
			}
		});
		when(tagDAOMock.selectTagsByEachNewsId(any(Collection.class))).thenAnswer(new Answer<Map<Long, Collection<Tag>>>() {
			@Override
			public Map<Long, Collection<Tag>> answer(InvocationOnMock invocation) throws Throwable {
				Map<Long, Collection<Tag>> map = new HashMap<Long, Collection<Tag>>();
				List<Tag> tags = new ArrayList<Tag>();
				Tag tag1 = new Tag(1L, "russian classical literature");
				Tag tag2 = new Tag(2L, "mertvie dushi");
				
				tags.add(tag1);
				map.put(1L, new ArrayList<Tag>(tags));
				tags.clear();
				tags.add(tag2);
				map.put(2L, new ArrayList<Tag>(tags));
				
				return map;
			}
		});
	}
	
	/**
	 * Adds the.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test
    public void add() throws ServiceException {
		long recievedId = tagService.add(new Tag("SomeTag"));
		Assert.assertEquals(5L, recievedId);
    }
	
	/**
	 * Gets the by id.
	 *
	 * @return the by id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getById() throws ServiceException {
		Tag Tag = new Tag();
		Tag.setTagName("russian classical literature");
		Assert.assertEquals(Tag, tagService.getById(1L));
    }
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAll() throws ServiceException {
		List<Tag> tagsExpected = new ArrayList<Tag>();
		Tag tag1 = new Tag(1L, "russian classical literature");
		Tag tag2 = new Tag(2L, "mertvie dushi");
		Tag tag3 = new Tag(3L, "18 vek");
		Tag tag4 = new Tag(4L, "history");
		tagsExpected.add(tag1); tagsExpected.add(tag2); tagsExpected.add(tag3); tagsExpected.add(tag4);
		Assert.assertEquals(tagsExpected, tagService.getAll());
    }
	
	/**
	 * Update.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void update() throws ServiceException, DAOException {
		Tag Tag = new Tag(1L, "russian classical literature ONLY!");
		tagService.update(Tag);
		verify(tagDAOMock).update(Tag);
	}
	
	/**
	 * Delete by id.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteById() throws ServiceException, DAOException {
		tagService.deleteById(1L);
		verify(tagDAOMock).deleteById(1L);
	}
	
	/**
	 * Gets the existing tags by names.
	 *
	 * @return the existing tags by names
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getExistingTagsByNames() throws ServiceException {
		List<String> tagNames = new ArrayList<String>();
		tagNames.add("russian classical literature"); tagNames.add("mertvie dushi");
		List<Tag> tagsExpected = new ArrayList<Tag>();
		Tag tag1 = new Tag(1L, "russian classical literature");
		Tag tag2 = new Tag(2L, "mertvie dushi");
		tagsExpected.add(tag1); tagsExpected.add(tag2);
		Assert.assertEquals(tagsExpected, tagService.getExistingTagsByNames(tagNames));
    }
	
	/**
	 * Adds the all tags.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void addAllTags() throws ServiceException, DAOException {
		List<Tag> tagsToAdd = new ArrayList<Tag>();
		Tag tag5 = new Tag(5L, "tag5");
		Tag tag6 = new Tag(6L, "tag6");
		tagsToAdd.add(tag5); tagsToAdd.add(tag6);
		
		tagService.addAllTags(tagsToAdd);
		verify(tagDAOMock).insertAllTags(tagsToAdd);
	}
	
	/**
	 * Adds the tags to news.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void addTagsToNews() throws ServiceException, DAOException {
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(5L); tagIds.add(6L);
		
		tagService.addTagsToNews(1L, tagIds);
		verify(tagDAOMock).insertTagsToNews(1L, tagIds);
	}
	
	/**
	 * Gets the tags by news id.
	 *
	 * @return the tags by news id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getTagsByNewsId() throws ServiceException {
		List<Tag> tagsExpected = new ArrayList<Tag>();
		Tag tag1 = new Tag(1L, "russian classical literature");
		tagsExpected.add(tag1);
		Assert.assertEquals(tagsExpected, tagService.getTagsByNewsId(1L));
    }
	
	
	/**
	 * Gets the tags by each news id.
	 *
	 * @return the tags by each news id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getTagsByEachNewsId() throws ServiceException {
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1L); tagIds.add(2L);
		
		Map<Long, Collection<Tag>> mapExpected = new HashMap<Long, Collection<Tag>>();
		List<Tag> tags = new ArrayList<Tag>();
		Tag tag1 = new Tag(1L, "russian classical literature");
		Tag tag2 = new Tag(2L, "mertvie dushi");
		
		tags.add(tag1);
		mapExpected.put(1L, new ArrayList<Tag>(tags));
		tags.clear();
		tags.add(tag2);
		mapExpected.put(2L, new ArrayList<Tag>(tags));
		
		Assert.assertEquals(mapExpected, tagService.getTagsByEachNewsId(tagIds));
    }	
	
	/**
	 * Delete tag from news.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteTagFromNews() throws ServiceException, DAOException {
		tagService.deleteTagFromNews(1L, 1L);
		verify(tagDAOMock).deleteTagFromNews(1L, 1L);
	}
	
	/**
	 * Delete all tags from news.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteAllTagsFromNews() throws ServiceException, DAOException {
		tagService.deleteAllTagsFromNews(1L);
		verify(tagDAOMock).deleteAllTagsFromNews(1L);
	}
}
