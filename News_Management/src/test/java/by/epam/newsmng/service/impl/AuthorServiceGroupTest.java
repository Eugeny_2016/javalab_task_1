package by.epam.newsmng.service.impl;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import by.epam.newsmng.dao.AuthorDAO;
import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.domain.Author;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthorServiceGroupTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceGroupTest {
	
	/** The author dao mock. */
	@Mock
	private AuthorDAO authorDAOMock;
	
	/** The author service. */
	@InjectMocks
	private AuthorServiceGroup authorService;
	
	/**
	 * Setup data.
	 *
	 * @throws DAOException the DAO exception
	 */
	@SuppressWarnings("unchecked")
	@Before
	public void setupData() throws DAOException {
		when(authorDAOMock.insert(new Author("SomeAuthor", null))).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) {
				return 4L;
			}
		});
		when(authorDAOMock.selectById(1L)).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Author author = new Author();
				author.setAuthorId(1L);
				author.setAuthorName("Pushkin");
				return author;
			}
		});
		when(authorDAOMock.selectAll()).thenAnswer(new Answer<Collection<Author>>() {
			@Override
			public Collection<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> authors = new ArrayList<Author>();
				Author author1 = new Author(1L, "Puskin", null);
				Author author2 = new Author(2L, "Lermontov", null);
				Author author3 = new Author(3L, "Gogol", null);
				authors.add(author1); authors.add(author2); authors.add(author3);
				return authors;
			}
		});
		when(authorDAOMock.selectExistingAuthorsByNames(any(Collection.class))).thenAnswer(new Answer<Collection<Author>>() {
			@Override
			public Collection<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> authors = new ArrayList<Author>();
				Author author1 = new Author(1L, "Pushkin", null);
				Author author2 = new Author(2L, "Lermontov", null);
				authors.add(author1); authors.add(author2);
				return authors;
			}
		});
		when(authorDAOMock.selectByName("Puskin")).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Author author = new Author();
				author.setAuthorId(1L);
				author.setAuthorName("Pushkin");
				return author;
			}
		});
		when(authorDAOMock.selectAuthorsByNewsId(1L)).thenAnswer(new Answer<Collection<Author>>() {
			@Override
			public Collection<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> authors = new ArrayList<Author>();
				Author author1 = new Author(1L, "Pushkin", null);
				Author author2 = new Author(2L, "Lermontov", null);
				authors.add(author1); authors.add(author2);
				return authors;
			}
		});
		when(authorDAOMock.selectAuthorsByEachNewsId(any(Collection.class))).thenAnswer(new Answer<Map<Long, Collection<Author>>>() {
			@Override
			public Map<Long, Collection<Author>> answer(InvocationOnMock invocation) throws Throwable {
				Map<Long, Collection<Author>> map = new HashMap<Long, Collection<Author>>();
				List<Author> authors = new ArrayList<Author>();
				Author author1 = new Author(1L, "Pushkin", null);
				Author author2 = new Author(2L, "Lermontov", null);
				authors.add(author1); authors.add(author2);
				map.put(1L, authors);
				return map;
			}
		});
	}
	
	/**
	 * Adds the.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test
    public void add() throws ServiceException {
		long recievedId = authorService.add(new Author("SomeAuthor", null));
		Assert.assertEquals(4L, recievedId);
    }
	
	/**
	 * Gets the by id.
	 *
	 * @return the by id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getById() throws ServiceException {
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("Pushkin");
		Assert.assertEquals(author, authorService.getById(1L));
    }
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAll() throws ServiceException {
		List<Author> authorsExpected = new ArrayList<Author>();
		Author author1 = new Author(1L, "Puskin", null);
		Author author2 = new Author(2L, "Lermontov", null);
		Author author3 = new Author(3L, "Gogol", null);
		authorsExpected.add(author1); authorsExpected.add(author2); authorsExpected.add(author3);
		Assert.assertEquals(authorsExpected, authorService.getAll());
    }
	
	/**
	 * Update.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void update() throws ServiceException, DAOException {
		Author author = new Author(1L, "Pushkin Aleksander", null);
		authorService.update(author);
		verify(authorDAOMock).update(author);
	}
	
	/**
	 * Delete by id.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteById() throws ServiceException, DAOException {
		authorService.deleteById(1L);
		verify(authorDAOMock).deleteById(1L);
	}
	
	/**
	 * Make author expired by id.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void makeAuthorExpiredById() throws ServiceException, DAOException {
		Timestamp tstmp = Timestamp.valueOf("2016-04-20 10:15:30.19");
		authorService.makeAuthorExpiredById(1L, tstmp);
		verify(authorDAOMock).updateAuthorExpiredById(1L, tstmp);
	}
	
	/**
	 * Gets the by name.
	 *
	 * @return the by name
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getByName() throws ServiceException {
		Author authorExpected = new Author();
		authorExpected.setAuthorId(1L);
		authorExpected.setAuthorName("Pushkin");
		Assert.assertEquals(authorExpected, authorService.getByName("Puskin"));
    }
	
	/**
	 * Gets the existing authors by names.
	 *
	 * @return the existing authors by names
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getExistingAuthorsByNames() throws ServiceException {
		List<String> authorNames = new ArrayList<String>();
		authorNames.add("Pushkin"); authorNames.add("Lermontov"); 
		List<Author> authorsExpected = new ArrayList<Author>();
		Author author1 = new Author(1L, "Pushkin", null);
		Author author2 = new Author(2L, "Lermontov", null);
		authorsExpected.add(author1); authorsExpected.add(author2);
		Assert.assertEquals(authorsExpected, authorService.getExistingAuthorsByNames(authorNames));
    }
	
	/**
	 * Adds the all authors.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void addAllAuthors() throws ServiceException, DAOException {
		List<Author> authorsToAdd = new ArrayList<Author>();
		Author author1 = new Author(4L, "SomeNewAuthor1", null);
		Author author2 = new Author(5L, "SomeNewAuthor1", null);
		authorsToAdd.add(author1); authorsToAdd.add(author2);
		
		authorService.addAllAuthors(authorsToAdd);
		verify(authorDAOMock).insertAllAuthors(authorsToAdd);
	}
	
	/**
	 * Adds the authors to news.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void addAuthorsToNews() throws ServiceException, DAOException {
		List<Long> authorIds = new ArrayList<Long>();
		authorIds.add(4L); authorIds.add(5L);
		
		authorService.addAuthorsToNews(new Long(1), authorIds);
		verify(authorDAOMock).insertAuthorsToNews(new Long(1), authorIds);
	}
	
	/**
	 * Gets the authors by news id.
	 *
	 * @return the authors by news id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAuthorsByNewsId() throws ServiceException {
		List<Author> authorsExpected = new ArrayList<Author>();
		Author author1 = new Author(1L, "Pushkin", null);
		Author author2 = new Author(2L, "Lermontov", null);
		authorsExpected.add(author1); authorsExpected.add(author2);
		Assert.assertEquals(authorsExpected, authorService.getAuthorsByNewsId(1L));
    }
	
	/**
	 * Gets the authors by each news id.
	 *
	 * @return the authors by each news id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAuthorsByEachNewsId() throws ServiceException {
		Map<Long, Collection<Author>> mapExpected = new HashMap<Long, Collection<Author>>();
		List<Author> authors = new ArrayList<Author>();
		List<Long> newsIds = new ArrayList<Long>();
		newsIds.add(1L);
		Author author1 = new Author(1L, "Pushkin", null);
		Author author2 = new Author(2L, "Lermontov", null);
		authors.add(author1); authors.add(author2);
		mapExpected.put(1L, authors);
		Assert.assertEquals(mapExpected, authorService.getAuthorsByEachNewsId(newsIds));
    }	
	
	/**
	 * Delete author from news.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteAuthorFromNews() throws ServiceException, DAOException {
		authorService.deleteAuthorFromNews(1L, 1L);
		verify(authorDAOMock).deleteAuthorFromNews(1L, 1L);
	}
	
	/**
	 * Delete all authors from news.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteAllAuthorsFromNews() throws ServiceException, DAOException {
		authorService.deleteAllAuthorsFromNews(1L);
		verify(authorDAOMock).deleteAllAuthorsFromNews(1L);
	}
}


