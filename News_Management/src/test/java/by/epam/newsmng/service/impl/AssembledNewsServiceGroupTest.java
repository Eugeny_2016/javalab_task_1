package by.epam.newsmng.service.impl;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import by.epam.newsmng.domain.AssembledNews;
import by.epam.newsmng.domain.Author;
import by.epam.newsmng.domain.Comment;
import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.Tag;
import by.epam.newsmng.service.AuthorService;
import by.epam.newsmng.service.CommentService;
import by.epam.newsmng.service.NewsService;
import by.epam.newsmng.service.ServiceException;
import by.epam.newsmng.service.TagService;

// TODO: Auto-generated Javadoc
/**
 * The Class AssembledNewsServiceGroupTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AssembledNewsServiceGroupTest {
	
	/** The news service mock. */
	@Mock
	private NewsService newsServiceMock;
	
	/** The author service mock. */
	@Mock
	private AuthorService authorServiceMock;
	
	/** The tag service mock. */
	@Mock
	private TagService tagServiceMock;
	
	/** The comment service mock. */
	@Mock
	private CommentService commentServiceMock;
	
	/** The assembled news service. */
	@InjectMocks
	private AssembledNewsServiceGroup assembledNewsService;
	
	/**
	 * Adds the.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test(expected = ServiceException.class)
    public void add() throws ServiceException {
		News news = new News(new Long(4), "Title4", "Short text4", "Full text4", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
		
		List<Author> authorsAdding = new ArrayList<Author>();
		List<Author> authorsExisting = new ArrayList<Author>();
		List<Author> authorsLeft = new ArrayList<Author>();
		List<String> authorNames = new ArrayList<String>();
		List<Tag> tags = new ArrayList<Tag>();
		List<Comment> comments = new ArrayList<Comment>();
		
		authorNames.add("Pushkin");
		authorNames.add("Lermontov");
		authorNames.add("Gogol");
		authorNames.add("Dostoevskiy");
		
		Author author1 = new Author(1L, "Pushkin", null);
		Author author2 = new Author(2L, "Lermontov", null);
		Author author3 = new Author(3L, "Gogol", null);
		Author author4 = new Author(4L, "Dostoevskiy", null);
		authorsAdding.add(author1); authorsAdding.add(author2); authorsAdding.add(author3); authorsAdding.add(author4);
		authorsExisting.add(author1); authorsExisting.add(author2); authorsExisting.add(author3);
		authorsLeft.add(author4);
		
		Tag tag1 = new Tag(1L, "russian classical literature");
		Tag tag2 = new Tag(2L, "mertvie dushi");
		Tag tag3 = new Tag(3L, "18 vek");
		Tag tag4 = new Tag(4L, "history");
		tags.add(tag1); tags.add(tag2); tags.add(tag3); tags.add(tag4);
		
		Comment comment1 = new Comment(1L, 1L, "Text1", null);
		Comment comment2 = new Comment(2L, 2L, "Text2", null);
		Comment comment3 = new Comment(3L, 3L, "Text3", null);
		comments.add(comment1); comments.add(comment2); comments.add(comment3);
		
		AssembledNews assembledNews = new AssembledNews(news, authorsAdding, tags, comments);
				
		when(newsServiceMock.add(news)).thenReturn(new Long(100500));
		when(authorServiceMock.getExistingAuthorsByNames(authorNames)).thenReturn(authorsExisting);
		doThrow(ServiceException.class).when(authorServiceMock).addAllAuthors(authorsLeft);
		
		assembledNewsService.add(assembledNews);
    }
}
