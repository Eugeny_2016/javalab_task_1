package by.epam.newsmng.service.impl;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.RoleDAO;
import by.epam.newsmng.domain.Role;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class RoleServiceGroupTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class RoleServiceGroupTest {
	
	/** The role dao mock. */
	@Mock
	private RoleDAO roleDAOMock;
	
	/** The role service. */
	@InjectMocks
	private RoleServiceGroup roleService;
	
	/**
	 * Setup data.
	 *
	 * @throws DAOException the DAO exception
	 */
	@Before
	public void setupData() throws DAOException {
		when(roleDAOMock.insert(new Role("newRole"))).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) {
				return 3L;
			}
		});
		when(roleDAOMock.selectById(1L)).thenAnswer(new Answer<Role>() {
			@Override
			public Role answer(InvocationOnMock invocation) throws Throwable {
				Role Role = new Role(1L, "administrator");
				return Role;
			}
		});
		when(roleDAOMock.selectAll()).thenAnswer(new Answer<Collection<Role>>() {
			@Override
			public Collection<Role> answer(InvocationOnMock invocation) throws Throwable {
				List<Role> roles = new ArrayList<Role>();
				Role role1 = new Role(1L, "administrator");
				Role role2 = new Role(1L, "reader");
				roles.add(role1); roles.add(role2);
				return roles;
			}
		});
		when(roleDAOMock.selectByName("administrator")).thenAnswer(new Answer<Role>() {
			@Override
			public Role answer(InvocationOnMock invocation) throws Throwable {
				Role Role = new Role(1L, "administrator");
				return Role;
			}
		});
	}
	
	/**
	 * Adds the.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test
    public void add() throws ServiceException {
		long recievedId = roleService.add(new Role("newRole"));
		Assert.assertEquals(3L, recievedId);
    }
	
	/**
	 * Gets the by id.
	 *
	 * @return the by id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getById() throws ServiceException {
		Role role = new Role(1L, "administrator");
		Assert.assertEquals(role, roleService.getById(1L));
    }
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAll() throws ServiceException {
		List<Role> rolesExpected = new ArrayList<Role>();
		Role role1 = new Role(1L, "administrator");
		Role role2 = new Role(1L, "reader");
		rolesExpected.add(role1); rolesExpected.add(role2);
		Assert.assertEquals(rolesExpected, roleService.getAll());
    }
	
	/**
	 * Update.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void update() throws ServiceException, DAOException {
		Role Role = new Role(1L, "adminus");
		roleService.update(Role);
		verify(roleDAOMock).update(Role);
	}
	
	/**
	 * Delete by id.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteById() throws ServiceException, DAOException {
		roleService.deleteById(1L);
		verify(roleDAOMock).deleteById(1L);
	}
	
	/**
	 * Gets the user by login.
	 *
	 * @return the user by login
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getUserByLogin() throws ServiceException {
		Role role = new Role(1L, "administrator");
		Assert.assertEquals(role, roleService.getByName("administrator"));
    }
}
