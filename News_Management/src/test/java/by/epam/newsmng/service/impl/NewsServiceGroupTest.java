package by.epam.newsmng.service.impl;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.NewsDAO;
import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.SearchCriteria;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class NewsServiceGroupTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceGroupTest {
	
	/** The news dao mock. */
	@Mock
	private NewsDAO newsDAOMock;
	
	/** The news service. */
	@InjectMocks
	private NewsServiceGroup newsService;
	
	/**
	 * Setup data.
	 *
	 * @throws DAOException the DAO exception
	 */
	@SuppressWarnings("unchecked")
	@Before
	public void setupData() throws DAOException {
		when(newsDAOMock.insert(new News("Title4", "Short text4", "Full text4", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20")))).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) {
				return 4L;
			}
		});
		when(newsDAOMock.selectById(1L)).thenAnswer(new Answer<News>() {
			@Override
			public News answer(InvocationOnMock invocation) throws Throwable {
				News news = new News("Title1", "Short text1", "Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
				return news;
			}
		});
		when(newsDAOMock.selectAll()).thenAnswer(new Answer<Collection<News>>() {
			@Override
			public Collection<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<News>();
				News news1 = new News("Title1", "Short text1", "Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
				News news2 = new News("Title2", "Short text2", "Full text2", Timestamp.valueOf("2016-04-20 15:10:30.10"), Date.valueOf("2016-04-20"));
				News news3 = new News("Title3", "Short text3", "Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), Date.valueOf("2016-04-20"));
				news.add(news1); news.add(news2); news.add(news3);
				return news;
			}
		});
		when(newsDAOMock.selectByTitle("Title1")).thenAnswer(new Answer<News>() {
			@Override
			public News answer(InvocationOnMock invocation) throws Throwable {
				News news1 = new News("Title1", "Short text1", "Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
				return news1;
			}
		});
		when(newsDAOMock.selectSeveralNews(any(Collection.class))).thenAnswer(new Answer<Collection<News>>() {
			@Override
			public Collection<News> answer(InvocationOnMock invocation) throws Throwable {
				Collection<News> listOfNews = new ArrayList<News>();
				News news2 = new News("Title2", "Short text2", "Full text2", Timestamp.valueOf("2016-04-20 15:10:30.10"), Date.valueOf("2016-04-20"));
				News news3 = new News("Title3", "Short text3", "Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), Date.valueOf("2016-04-20"));
				listOfNews.add(news2); listOfNews.add(news3);
				return listOfNews;
			}
		});
		when(newsDAOMock.selectNextN_News(3, 1)).thenAnswer(new Answer<Collection<News>>() {
			@Override
			public Collection<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<News>();
				News news3 = new News("Title3", "Short text3", "Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), Date.valueOf("2016-04-20"));
				News news2 = new News("Title2", "Short text2", "Full text2", Timestamp.valueOf("2016-04-20 15:10:30.10"), Date.valueOf("2016-04-20"));
				news.add(news3); news.add(news2);
				return news;
			}
		});
		when(newsDAOMock.selectBySearchCriteria(any(SearchCriteria.class))).thenAnswer(new Answer<Collection<News>>() {
			@Override
			public Collection<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<News>();
				News news3 = new News("Title3", "Short text3", "Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), Date.valueOf("2016-04-20"));
				news.add(news3);
				return news;
			}
		});
		when(newsDAOMock.selectAmountOfNews()).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				return 3L;
			}
		});
		when(newsDAOMock.selectAmountOfNewsBySearchCriteria(any(SearchCriteria.class))).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				return 1L;
			}
		});
	}
	
	/**
	 * Adds the.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test
    public void add() throws ServiceException {
		long recievedId = newsService.add(new News("Title4", "Short text4", "Full text4", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20")));
		Assert.assertEquals(4L, recievedId);
    }
	
	/**
	 * Gets the by id.
	 *
	 * @return the by id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getById() throws ServiceException {
		News News = new News("Title1", "Short text1", "Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
		Assert.assertEquals(News, newsService.getById(1L));
    }
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAll() throws ServiceException {
		List<News> newsExpected = new ArrayList<News>();
		News news1 = new News("Title1", "Short text1", "Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
		News news2 = new News("Title2", "Short text2", "Full text2", Timestamp.valueOf("2016-04-20 15:10:30.10"), Date.valueOf("2016-04-20"));
		News news3 = new News("Title3", "Short text3", "Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), Date.valueOf("2016-04-20"));
		newsExpected.add(news1); newsExpected.add(news2); newsExpected.add(news3);
		Assert.assertEquals(newsExpected, newsService.getAll());
    }
	
	/**
	 * Update.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void update() throws ServiceException, DAOException {
		News news = new News("Title111", "Short text1", "Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
		newsService.update(news);
		verify(newsDAOMock).update(news);
	}
	
	/**
	 * Delete by id.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteById() throws ServiceException, DAOException {
		newsService.deleteById(1L);
		verify(newsDAOMock).deleteById(1L);
	}
	
	/**
	 * Gets the by title.
	 *
	 * @return the by title
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void getByTitle() throws ServiceException, DAOException {
		News news1 = new News("Title1", "Short text1", "Full text1", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
		Assert.assertEquals(news1, newsService.getByTitle("Title1"));
	}
	
	/**
	 * Gets the several news.
	 *
	 * @return the several news
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void getSeveralNews() throws ServiceException, DAOException {
		Collection<News> listOfNewsExpected = new ArrayList<News>();
		News news2 = new News("Title2", "Short text2", "Full text2", Timestamp.valueOf("2016-04-20 15:10:30.10"), Date.valueOf("2016-04-20"));
		News news3 = new News("Title3", "Short text3", "Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), Date.valueOf("2016-04-20"));
		listOfNewsExpected.add(news2); listOfNewsExpected.add(news3);
		List<Long> newsIds = new ArrayList<Long>();
		newsIds.add(new Long(2)); newsIds.add(new Long(3));
		Assert.assertEquals(listOfNewsExpected, newsService.getSeveralNews(newsIds));
	}
	
	/**
	 * Gets the next n_ news.
	 *
	 * @return the next n_ news
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getNextN_News() throws ServiceException {
		List<News> newsExpected = new ArrayList<News>();
		News news3 = new News("Title3", "Short text3", "Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), Date.valueOf("2016-04-20"));
		News news2 = new News("Title2", "Short text2", "Full text2", Timestamp.valueOf("2016-04-20 15:10:30.10"), Date.valueOf("2016-04-20"));
		newsExpected.add(news3); newsExpected.add(news2);
		Assert.assertEquals(newsExpected, newsService.getNextN_News(3, 1));
    }
	
	/**
	 * Gets the news by search criteria.
	 *
	 * @return the news by search criteria
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getNewsBySearchCriteria() throws ServiceException {
		List<News> newsExpected = new ArrayList<News>();
		News news3 = new News("Title3", "Short text3", "Full text3", Timestamp.valueOf("2016-04-20 22:10:30.10"), Date.valueOf("2016-04-20"));
		newsExpected.add(news3);
		
		List<Long> authorIds = new ArrayList<Long>();
		authorIds.add(2L); authorIds.add(3L);
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(3L); tagIds.add(4L);
		SearchCriteria sc = new SearchCriteria(authorIds, tagIds);
		Assert.assertEquals(newsExpected, newsService.getNewsBySearchCriteria(sc));
    }
	
	/**
	 * Delete by title.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteByTitle() throws ServiceException, DAOException {
		newsService.deleteByTitle("Title1");
		verify(newsDAOMock).deleteByTitle("Title1");
	}
	
	/**
	 * Count all news.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test
    public void countAllNews() throws ServiceException {
		Assert.assertEquals(3L, newsService.countAllNews());
    }
	
	/**
	 * Count news by search criteria.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test
    public void countNewsBySearchCriteria() throws ServiceException {
		List<Long> authorIds = new ArrayList<Long>();
		authorIds.add(2L); authorIds.add(3L);
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(3L); tagIds.add(4L);
		SearchCriteria sc = new SearchCriteria(authorIds, tagIds);
		Assert.assertEquals(1L, newsService.countNewsBySearchCriteria(sc));
    }
}
