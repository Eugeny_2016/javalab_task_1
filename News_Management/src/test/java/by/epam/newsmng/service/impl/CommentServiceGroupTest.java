package by.epam.newsmng.service.impl;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import by.epam.newsmng.dao.CommentDAO;
import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.domain.Comment;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class CommentServiceGroupTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceGroupTest {
	
	/** The comment dao mock. */
	@Mock
	private CommentDAO commentDAOMock;
	
	/** The comment service. */
	@InjectMocks
	private CommentServiceGroup commentService;
	
	/**
	 * Setup data.
	 *
	 * @throws DAOException the DAO exception
	 */
	@SuppressWarnings("unchecked")
	@Before
	public void setupData() throws DAOException {
		when(commentDAOMock.insert(new Comment(1L, "Text4", null))).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) {
				return 4L;
			}
		});
		when(commentDAOMock.selectById(1L)).thenAnswer(new Answer<Comment>() {
			@Override
			public Comment answer(InvocationOnMock invocation) throws Throwable {
				Comment comment = new Comment(1L, 1L, "Text1", null);
				return comment;
			}
		});
		when(commentDAOMock.selectAll()).thenAnswer(new Answer<Collection<Comment>>() {
			@Override
			public Collection<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> comments = new ArrayList<Comment>();
				Comment comment1 = new Comment(1L, 1L, "Text1", null);
				Comment comment2 = new Comment(2L, 2L, "Text2", null);
				Comment comment3 = new Comment(3L, 3L, "Text3", null);
				comments.add(comment1); comments.add(comment2); comments.add(comment3);
				return comments;
			}
		});
		when(commentDAOMock.selectAllCommentsByNewsId(1L)).thenAnswer(new Answer<Collection<Comment>>() {
			@Override
			public Collection<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> comments = new ArrayList<Comment>();
				Comment comment1 = new Comment(1L, 1L, "Text1", null);
				comments.add(comment1);
				return comments;
			}
		});
		when(commentDAOMock.selectCommentsByEachNewsId(any(Collection.class))).thenAnswer(new Answer<Map<Long, Collection<Comment>>>() {
			@Override
			public Map<Long, Collection<Comment>> answer(InvocationOnMock invocation) throws Throwable {
				Map<Long, Collection<Comment>> map = new HashMap<Long, Collection<Comment>>();
				List<Comment> comments = new ArrayList<Comment>();
				Comment comment1 = new Comment(1L, 1L, "Text1", null);
				comments.add(comment1);
				map.put(1L, comments);
				return map;
			}
		});
		when(commentDAOMock.selectNMostCommentedNewsId(3)).thenAnswer(new Answer<Collection<Comment>>() {
			@Override
			public Collection<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> comments = new ArrayList<Comment>();
				Comment comment1 = new Comment(1L, 1L, "Text1", null);
				Comment comment2 = new Comment(2L, 2L, "Text2", null);
				Comment comment3 = new Comment(3L, 3L, "Text3", null);
				comments.add(comment1); comments.add(comment2); comments.add(comment3);
				return comments;
			}
		});
	}
	
	/**
	 * Adds the.
	 *
	 * @throws ServiceException the service exception
	 */
	@Test
    public void add() throws ServiceException {
		long recievedId = commentService.add(new Comment(1L, "Text4", null));
		Assert.assertEquals(4L, recievedId);
    }
	
	/**
	 * Gets the by id.
	 *
	 * @return the by id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getById() throws ServiceException {
		Comment comment = new Comment(1L, 1L, "Text1", null);
		Assert.assertEquals(comment, commentService.getById(1L));
    }
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAll() throws ServiceException {
		List<Comment> commentsExpected = new ArrayList<Comment>();
		Comment comment1 = new Comment(1L, 1L, "Text1", null);
		Comment comment2 = new Comment(2L, 2L, "Text2", null);
		Comment comment3 = new Comment(3L, 3L, "Text3", null);
		commentsExpected.add(comment1); commentsExpected.add(comment2); commentsExpected.add(comment3);
		Assert.assertEquals(commentsExpected, commentService.getAll());
    }
	
	/**
	 * Update.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void update() throws ServiceException, DAOException {
		Comment Comment = new Comment(1L, 1L, "Text1111", null);
		commentService.update(Comment);
		verify(commentDAOMock).update(Comment);
	}
	
	/**
	 * Delete by id.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteById() throws ServiceException, DAOException {
		commentService.deleteById(1L);
		verify(commentDAOMock).deleteById(1L);
	}
	
	/**
	 * Gets the all comments by news id.
	 *
	 * @return the all comments by news id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getAllCommentsByNewsId() throws ServiceException {
		List<Comment> commentsExpected = new ArrayList<Comment>();
		Comment comment1 = new Comment(1L, 1L, "Text1", null);
		commentsExpected.add(comment1);
		Assert.assertEquals(commentsExpected, commentService.getCommentsByNewsId(1L));
    }
	
	/**
	 * Censure comment.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void censureComment() throws ServiceException, DAOException {
		String newString = "Some reason";
		commentService.censureComment(1L, newString);
		verify(commentDAOMock).updateCommentWithCensuring(1L, newString);
	}
	
	/**
	 * Gets the comments by each news id.
	 *
	 * @return the comments by each news id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getCommentsByEachNewsId() throws ServiceException {
		List<Long> newsIds = new ArrayList<Long>();
		newsIds.add(1L);
		Map<Long, Collection<Comment>> mapExpected = new HashMap<Long, Collection<Comment>>();
		List<Comment> comments = new ArrayList<Comment>();
		Comment comment1 = new Comment(1L, 1L, "Text1", null);
		comments.add(comment1);
		mapExpected.put(1L, comments);
		Assert.assertEquals(mapExpected, commentService.getCommentsByEachNewsId(newsIds));
    }
	
	/**
	 * Delete comment from news.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteCommentFromNews() throws ServiceException, DAOException {
		commentService.deleteCommentFromNews(1L, 1L);
		verify(commentDAOMock).deleteCommentFromNews(1L, 1L);
	}
	
	/**
	 * Delete all comments from news.
	 *
	 * @throws ServiceException the service exception
	 * @throws DAOException the DAO exception
	 */
	@Test
	public void deleteAllCommentsFromNews() throws ServiceException, DAOException {
		commentService.deleteCommentsFromNews(1L);
		verify(commentDAOMock).deleteAllCommentsFromNews(1L);
	}
	
	/**
	 * Gets the n most commented news id.
	 *
	 * @return the n most commented news id
	 * @throws ServiceException the service exception
	 */
	@Test
    public void getNMostCommentedNewsId() throws ServiceException {
		List<Comment> commentsExpected = new ArrayList<Comment>();
		Comment comment1 = new Comment(1L, 1L, "Text1", null);
		Comment comment2 = new Comment(2L, 2L, "Text2", null);
		Comment comment3 = new Comment(3L, 3L, "Text3", null);
		commentsExpected.add(comment1); commentsExpected.add(comment2); commentsExpected.add(comment3);
		Assert.assertEquals(commentsExpected, commentService.getNMostCommentedNewsId(3));
    }
}
