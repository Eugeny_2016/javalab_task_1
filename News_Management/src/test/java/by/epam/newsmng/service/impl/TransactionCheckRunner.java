package by.epam.newsmng.service.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import by.epam.newsmng.domain.AssembledNews;
import by.epam.newsmng.domain.Author;
import by.epam.newsmng.domain.Comment;
import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.Tag;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class TransactionCheckRunner.
 */

/* this is a class for testing transaction methods of AssembledNewsServiceGroup class
 * before running main-method insert line
 * if (true) { throw new ServiceException("ups"); }
 * into any transaction method of AssembledNewsServiceGroup class
 */
public class TransactionCheckRunner {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws ServiceException the service exception
	 */
	public static void main(String[] args) throws ServiceException {
		Locale.setDefault(Locale.ENGLISH);
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("spring_test.xml");
		
		News news = new News(new Long(4), "Title4", "Short text4", "Full text4", Timestamp.valueOf("2016-04-20 12:10:30.10"), Date.valueOf("2016-04-20"));
		
		List<Author> authorsAdding = new ArrayList<Author>();
		List<Author> authorsExisting = new ArrayList<Author>();
		List<Author> authorsLeft = new ArrayList<Author>();
		List<String> authorNames = new ArrayList<String>();
		List<Tag> tags = new ArrayList<Tag>();
		List<Comment> comments = new ArrayList<Comment>();
		
		authorNames.add("Pushkin");
		authorNames.add("Lermontov");
		authorNames.add("Gogol");
		authorNames.add("Dostoevskiy");
		
		Author author1 = new Author(1L, "Pushkin", null);
		Author author2 = new Author(2L, "Lermontov", null);
		Author author3 = new Author(3L, "Gogol", null);
		Author author4 = new Author(4L, "Dostoevskiy", null);
		authorsAdding.add(author1); authorsAdding.add(author2); authorsAdding.add(author3); authorsAdding.add(author4);
		authorsExisting.add(author1); authorsExisting.add(author2); authorsExisting.add(author3);
		authorsLeft.add(author4);
		
		Tag tag1 = new Tag(1L, "russian classical literature");
		Tag tag2 = new Tag(2L, "mertvie dushi");
		Tag tag3 = new Tag(3L, "18 vek");
		Tag tag4 = new Tag(4L, "history");
		tags.add(tag1); tags.add(tag2); tags.add(tag3); tags.add(tag4);
		
		Comment comment1 = new Comment(1L, 1L, "Text1", null);
		Comment comment2 = new Comment(2L, 2L, "Text2", null);
		Comment comment3 = new Comment(3L, 3L, "Text3", null);
		comments.add(comment1); comments.add(comment2); comments.add(comment3);
		
		AssembledNews assembledNews = new AssembledNews(news, authorsAdding, tags, comments);
		AssembledNewsServiceGroup assembledNewsService = context.getBean(AssembledNewsServiceGroup.class);
		assembledNewsService.add(assembledNews);
		
		context.close();
	}
}
