package by.epam.newsmng.constant;

// TODO: Auto-generated Javadoc
/**
 * The Class DBColumnName.
 */
public class DBColumnName {
		
	/** The Constant AUTHOR_ID. */
	public static final String AUTHOR_ID = "AUTHOR_ID";
	
	/** The Constant AUTHOR_AUTHOR_ID. */
	public static final String AUTHOR_AUTHOR_ID = "AUTHOR_AUTHOR_ID";
	
	/** The Constant AUTHOR_NAME. */
	public static final String AUTHOR_NAME = "AUTHOR_NAME";
	
	/** The Constant AUTHOR_EXPIRED. */
	public static final String AUTHOR_EXPIRED = "EXPIRED";
	
	/** The Constant COMMENT_ID. */
	public static final String COMMENT_ID = "COMMENT_ID";
	
	/** The Constant COMMENT_NEWS_ID. */
	public static final String COMMENT_NEWS_ID = "NEWS_NEWS_ID";
	
	/** The Constant COMMENT_TEXT. */
	public static final String COMMENT_TEXT = "COMMENT_TEXT";
	
	/** The Constant COMMENT_CREATION_DATE. */
	public static final String COMMENT_CREATION_DATE = "CREATION_DATE";
	
	/** The Constant NEWS_ID. */
	public static final String NEWS_ID = "NEWS_ID";
	
	/** The Constant NEWS_NEWS_ID. */
	public static final String NEWS_NEWS_ID = "NEWS_NEWS_ID";
	
	/** The Constant NEWS_TITLE. */
	public static final String NEWS_TITLE = "TITLE";
	
	/** The Constant NEWS_SHORT_TEXT. */
	public static final String NEWS_SHORT_TEXT = "SHORT_TEXT";
	
	/** The Constant NEWS_FULL_TEXT. */
	public static final String NEWS_FULL_TEXT = "FULL_TEXT";
	
	/** The Constant NEWS_CREATION_DATE. */
	public static final String NEWS_CREATION_DATE = "CREATION_DATE";
	
	/** The Constant NEWS_MODIFICATION_DATE. */
	public static final String NEWS_MODIFICATION_DATE = "MODIFICATION_DATE";
	
	/** The Constant ROLE_ID. */
	public static final String ROLE_ID = "ROLE_ID";
	
	/** The Constant ROLE_ROLE_ID. */
	public static final String ROLE_ROLE_ID = "ROLE_ROLE_ID";
	
	/** The Constant ROLE_NAME. */
	public static final String ROLE_NAME = "ROLE_NAME";
	
	/** The Constant TAG_ID. */
	public static final String TAG_ID = "TAG_ID";
	
	/** The Constant TAG_TAG_ID. */
	public static final String TAG_TAG_ID = "TAG_TAG_ID";
	
	/** The Constant TAG_NAME. */
	public static final String TAG_NAME = "TAG_NAME";
	
	/** The Constant USER_ID. */
	public static final String USER_ID = "USER_ID";
	
	/** The Constant USER_NAME. */
	public static final String USER_NAME = "USER_NAME";
	
	/** The Constant USER_LOGIN. */
	public static final String USER_LOGIN = "LOGIN";
	
	/** The Constant USER_PASSWORD. */
	public static final String USER_PASSWORD = "PASSWORD";

}
