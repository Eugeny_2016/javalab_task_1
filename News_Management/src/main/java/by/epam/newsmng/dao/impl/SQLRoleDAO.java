package by.epam.newsmng.dao.impl;

import static by.epam.newsmng.constant.DBColumnName.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.RoleDAO;
import by.epam.newsmng.domain.Role;

// TODO: Auto-generated Javadoc
/**
 * The Class SQLRoleDAO.
 */
@Repository
public class SQLRoleDAO implements RoleDAO {
	
	/** The Constant ROLE_SQL_INSERT. */
	private static final String ROLE_SQL_INSERT = "INSERT INTO ROLE (ROLE_NAME) VALUES(?)";
	
	/** The Constant ROLE_BY_ID_SQL_SELECT. */
	private static final String ROLE_BY_ID_SQL_SELECT = "SELECT ROLE_ID, ROLE_NAME "
													  + "FROM ROLE "
													  + "WHERE ROLE_ID = ?";
	
	/** The Constant ROLE_ALL_SQL_SELECT. */
	private static final String ROLE_ALL_SQL_SELECT = "SELECT ROLE_ID, ROLE_NAME "
												    + "FROM ROLE";
	
	/** The Constant ROLE_NAME_SQL_UPDATE. */
	private static final String ROLE_NAME_SQL_UPDATE = "UPDATE ROLE "
													 + "SET ROLE_NAME = ? "
													 + "WHERE ROLE_ID = ?";
	
	/** The Constant ROLE_BY_ID_SQL_DELETE. */
	private static final String ROLE_BY_ID_SQL_DELETE = "DELETE FROM ROLE "
													  + "WHERE ROLE_ID = ?";
	
	/** The Constant ROLE_BY_NAME_SQL_SELECT. */
	private static final String ROLE_BY_NAME_SQL_SELECT = "SELECT ROLE_ID, ROLE_NAME "
			  											+ "FROM ROLE "
			  											+ "WHERE ROLE_NAME = ? ";
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * Sets the data source.
	 *
	 * @param dataSource the new data source
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#insert(java.lang.Object)
	 */
	@Override
	public Long insert(Role role) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long roleId = null;
		String[] columnsToReturn = {ROLE_ID};
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(ROLE_SQL_INSERT, columnsToReturn);
			ps.setString(1, role.getRoleName());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			
			while (rs.next()) {
				roleId = rs.getLong(1);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting a role. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return roleId;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectById(java.lang.Long)
	 */
	@Override
	public Role selectById(Long roleId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Role role = new Role();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(ROLE_BY_ID_SQL_SELECT);
			ps.setLong(1, roleId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				role.setRoleId(rs.getLong(ROLE_ID));
				role.setRoleName(rs.getString(ROLE_NAME));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting a role by id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return role;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectAll()
	 */
	@Override
	public Collection<Role> selectAll() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<Role> listOfRoles = new ArrayList<Role>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(ROLE_ALL_SQL_SELECT);
			
			while (rs.next()) {
				Role Role = new Role();
				Role.setRoleId(rs.getLong(ROLE_ID));
				Role.setRoleName(rs.getString(ROLE_NAME));
				listOfRoles.add(Role);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of roles. ", ex);
		} finally {
			closeStatement(st);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfRoles;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Role role) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(ROLE_NAME_SQL_UPDATE);
			ps.setString(1, role.getRoleName());
			ps.setLong(2, role.getRoleId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while updating the role's name. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long roleId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(ROLE_BY_ID_SQL_DELETE);
			ps.setLong(1, roleId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting the role. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.RoleDAO#selectByName(java.lang.String)
	 */
	@Override
	public Role selectByName(String name) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Role role = new Role();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(ROLE_BY_NAME_SQL_SELECT);
			ps.setString(1, name);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				role.setRoleId(rs.getLong(ROLE_ID));
				role.setRoleName(rs.getString(ROLE_NAME));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting the role by name. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return role;
	}

}
