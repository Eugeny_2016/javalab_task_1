package by.epam.newsmng.dao;

import java.util.Collection;

import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.SearchCriteria;

// TODO: Auto-generated Javadoc
/**
 * The Interface NewsDAO.
 */
public interface NewsDAO extends CrudDAO<News> {
	
	/**
	 * Select by title.
	 *
	 * @param newsTitle the news title
	 * @return the news
	 * @throws DAOException the DAO exception
	 */
	public News selectByTitle(String newsTitle) throws DAOException;
	
	/**
	 * Select several news.
	 *
	 * @param newsIds the news ids
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<News> selectSeveralNews(Collection<Long> newsIds) throws DAOException;
	
	/**
	 * Select next n_ news.
	 *
	 * @param maxValue the max value
	 * @param minValue the min value
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<News> selectNextN_News(long maxValue, long minValue) throws DAOException;
	
	/**
	 * Select by search criteria.
	 *
	 * @param searchCriteria the search criteria
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<News> selectBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;
	
	/**
	 * Delete by title.
	 *
	 * @param newsTitle the news title
	 * @throws DAOException the DAO exception
	 */
	public void deleteByTitle(String newsTitle) throws DAOException;
	
	/**
	 * Select amount of news.
	 *
	 * @return the long
	 * @throws DAOException the DAO exception
	 */
	public long selectAmountOfNews() throws DAOException;
	
	/**
	 * Select amount of news by search criteria.
	 *
	 * @param searchCriteria the search criteria
	 * @return the long
	 * @throws DAOException the DAO exception
	 */
	public long selectAmountOfNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;
	
}
