package by.epam.newsmng.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsmng.dao.CommentDAO;
import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.domain.Comment;
import static by.epam.newsmng.constant.DBColumnName.*;

// TODO: Auto-generated Javadoc
/**
 * The Class SQLCommentDAO.
 */
@Repository
public class SQLCommentDAO implements CommentDAO {
	
	/** The Constant COMMENT_SQL_INSERT. */
	private static final String COMMENT_SQL_INSERT = "INSERT INTO COMMENTS (NEWS_NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES(?,?,?)";
	
	/** The Constant COMMENT_BY_ID_SQL_SELECT. */
	private static final String COMMENT_BY_ID_SQL_SELECT = "SELECT COMMENT_ID, NEWS_NEWS_ID, COMMENT_TEXT, CREATION_DATE "
														 + "FROM COMMENTS "
														 + "WHERE COMMENT_ID = ?";
	
	/** The Constant COMMENT_ALL_SQL_SELECT. */
	private static final String COMMENT_ALL_SQL_SELECT = "SELECT COMMENT_ID, NEWS_NEWS_ID, COMMENT_TEXT, CREATION_DATE "
													   + "FROM COMMENTS";
	
	/** The Constant COMMENT_TEXT_SQL_UPDATE. */
	private static final String COMMENT_TEXT_SQL_UPDATE = "UPDATE COMMENTS "
													    + "SET COMMENT_TEXT = ? "
												    	+ "WHERE COMMENT_ID = ?";
	
	/** The Constant COMMENT_BY_ID_SQL_DELETE. */
	private static final String COMMENT_BY_ID_SQL_DELETE = "DELETE FROM COMMENTS "
														 + "WHERE COMMENT_ID = ?";
	
	/** The Constant COMMENT_ALL_BY_NEWS_ID_SQL_SELECT. */
	private static final String COMMENT_ALL_BY_NEWS_ID_SQL_SELECT = "SELECT COMMENT_ID, NEWS_NEWS_ID, COMMENT_TEXT, CREATION_DATE "
																  + "FROM COMMENTS "
																  + "WHERE NEWS_NEWS_ID = ?"
																  + "ORDER BY COMMENT_ID ASC";
	
	/** The Constant COMMENT_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1. */
	private static final String COMMENT_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1 = "SELECT NEWS_NEWS_ID, COMMENT_ID, COMMENT_TEXT, CREATION_DATE "
																			    + "FROM COMMENTS "
																			    + "WHERE NEWS_NEWS_ID IN (";
		 	   
	/** The Constant COMMENT_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2. */
	private static final String COMMENT_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2 = ") ORDER BY NEWS_NEWS_ID ASC, COMMENT_ID ASC";
	
	/** The Constant COMMENT_BY_NEWS_ID_SQL_DELETE. */
	private static final String COMMENT_BY_NEWS_ID_SQL_DELETE = "DELETE FROM COMMENTS "
															  + "WHERE NEWS_NEWS_ID = ? AND COMMENT_ID = ?";
	
	/** The Constant COMMENT_ALL_BY_NEWS_ID_SQL_DELETE. */
	private static final String COMMENT_ALL_BY_NEWS_ID_SQL_DELETE = "DELETE FROM COMMENTS "
															  	  + "WHERE NEWS_NEWS_ID = ?";
	
	/** The Constant COMMENT_FOR_N_MOST_POPULAR_NEWS_COUNT. */
	private static final String COMMENT_FOR_N_MOST_POPULAR_NEWS_COUNT = "SELECT * FROM "
																	  + "(SELECT NEWS_NEWS_ID, COUNT(COMMENT_ID) AS AMOUNT_OF_COMMENTS "
																  	  + "FROM COMMENTS "
																  	  + "GROUP BY NEWS_NEWS_ID "
																  	  + "ORDER BY AMOUNT_OF_COMMENTS DESC) "
																	  + "WHERE ROWNUM <= ?";
	
	/** The default censure text. */
	@Value("${comment.text}")
	private String defaultCensureText;
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * Sets the data source.
	 *
	 * @param dataSource the new data source
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/**
	 * Sets the default censure text.
	 *
	 * @param defaultCensureText the new default censure text
	 */
	public void setDefaultCensureText(String defaultCensureText) {
		this.defaultCensureText = defaultCensureText;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#insert(java.lang.Object)
	 */
	@Override
	public Long insert(Comment comment) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long commentId = null;
		String[] columnsToReturn = {COMMENT_ID};
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_SQL_INSERT, columnsToReturn);
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, comment.getCreationDate());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			
			while (rs.next()) {
				commentId = rs.getLong(1);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting a comment. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return commentId;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectById(java.lang.Long)
	 */
	@Override
	public Comment selectById(Long commentId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Comment comment = new Comment();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_BY_ID_SQL_SELECT);
			ps.setLong(1, commentId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				comment.setCommentId(rs.getLong(COMMENT_ID));
				comment.setNewsId(rs.getLong(COMMENT_NEWS_ID));
				comment.setCommentText(rs.getString(COMMENT_TEXT));
				comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting a comment by id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return comment;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectAll()
	 */
	@Override
	public Collection<Comment> selectAll() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<Comment> listOfComments = new ArrayList<Comment>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(COMMENT_ALL_SQL_SELECT);
			
			while (rs.next()) {
				Comment comment = new Comment();
				comment.setCommentId(rs.getLong(COMMENT_ID));
				comment.setNewsId(rs.getLong(COMMENT_NEWS_ID));
				comment.setCommentText(rs.getString(COMMENT_TEXT));
				comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
				listOfComments.add(comment);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of comments. ", ex);
		} finally {
			closeStatement(st);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfComments;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Comment comment) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_TEXT_SQL_UPDATE);
			ps.setString(1, comment.getCommentText());
			ps.setLong(2, comment.getCommentId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while updating comment's text. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long commentId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_BY_ID_SQL_DELETE);
			ps.setLong(1, commentId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting a comment. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CommentDAO#selectAllCommentsByNewsId(java.lang.Long)
	 */
	@Override
	public Collection<Comment> selectAllCommentsByNewsId(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Comment> listOfComments = new ArrayList<Comment>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_ALL_BY_NEWS_ID_SQL_SELECT);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Comment comment = new Comment();
				comment.setCommentId(rs.getLong(COMMENT_ID));
				comment.setNewsId(rs.getLong(COMMENT_NEWS_ID));
				comment.setCommentText(rs.getString(COMMENT_TEXT));
				comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
				listOfComments.add(comment);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of comments by news id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfComments;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CommentDAO#updateCommentWithCensuring(java.lang.Long, java.lang.String)
	 */
	@Override
	public void updateCommentWithCensuring(Long commentId, String messageWithReason) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_TEXT_SQL_UPDATE);
			ps.setString(1, defaultCensureText + messageWithReason);
			ps.setLong(2, commentId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while censuring comment's text. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CommentDAO#selectCommentsByEachNewsId(java.util.Collection)
	 */
	@Override
	public Map<Long, Collection<Comment>> selectCommentsByEachNewsId(Collection<Long> newsIds) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder inRange = new StringBuilder();
		Collection<Comment> commentsOfCurrentNews = null;
		Map<Long, Collection<Comment>> mapOfNewsIdsAndComments = new LinkedHashMap<Long, Collection<Comment>>();
		
		inRange.append(COMMENT_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1);
		for (Long id : newsIds) {
			inRange.append(id);
			inRange.append(",");
		}
		inRange.deleteCharAt(inRange.length()-1);
		inRange.append(COMMENT_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2);
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(inRange.toString());
			rs = ps.executeQuery();
			
			Long prevNewsId = null;
			if (rs.next()) {
				prevNewsId = rs.getLong(NEWS_NEWS_ID);
				Comment comment = new Comment();
				comment.setCommentId(rs.getLong(COMMENT_ID));
				comment.setNewsId(prevNewsId);
				comment.setCommentText(rs.getString(COMMENT_TEXT));
				comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
				commentsOfCurrentNews = new ArrayList<Comment>();
				commentsOfCurrentNews.add(comment);
			}
			
			while (rs.next()) {
				Long newsId = rs.getLong(NEWS_NEWS_ID);
				if (newsId.equals(prevNewsId)) {
					Comment comment = new Comment();
					comment.setCommentId(rs.getLong(COMMENT_ID));
					comment.setNewsId(prevNewsId);
					comment.setCommentText(rs.getString(COMMENT_TEXT));
					comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
					commentsOfCurrentNews.add(comment);
				} else {
					mapOfNewsIdsAndComments.put(prevNewsId, commentsOfCurrentNews);
					prevNewsId = rs.getLong(NEWS_NEWS_ID);
					Comment comment = new Comment();
					comment.setCommentId(rs.getLong(COMMENT_ID));
					comment.setNewsId(prevNewsId);
					comment.setCommentText(rs.getString(COMMENT_TEXT));
					comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
					commentsOfCurrentNews = new ArrayList<Comment>();
					commentsOfCurrentNews.add(comment);
				}
				mapOfNewsIdsAndComments.put(prevNewsId, commentsOfCurrentNews);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of comments by each news id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return mapOfNewsIdsAndComments;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CommentDAO#deleteCommentFromNews(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteCommentFromNews(Long newsId, Long commentId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_BY_NEWS_ID_SQL_DELETE);
			ps.setLong(1, newsId);
			ps.setLong(2, commentId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting a comment by news id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CommentDAO#deleteAllCommentsFromNews(java.lang.Long)
	 */
	@Override
	public void deleteAllCommentsFromNews(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_ALL_BY_NEWS_ID_SQL_DELETE);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting all comments by news id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CommentDAO#selectNMostCommentedNewsId(int)
	 */
	@Override
	public Collection<Long> selectNMostCommentedNewsId(int amountOfNewsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection<Long> nMostCommentedNewsId = new ArrayList<Long>();
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(COMMENT_FOR_N_MOST_POPULAR_NEWS_COUNT);
			ps.setInt(1, amountOfNewsId);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				nMostCommentedNewsId.add(rs.getLong(COMMENT_NEWS_ID));
			}			
			
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting " + amountOfNewsId + " most commented news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return nMostCommentedNewsId;
	}	
}
