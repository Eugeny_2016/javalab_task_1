package by.epam.newsmng.dao;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Map;

import by.epam.newsmng.domain.Author;

// TODO: Auto-generated Javadoc
/**
 * The Interface AuthorDAO.
 */
public interface AuthorDAO extends CrudDAO<Author> {
	
	/**
	 * Update author expired by id.
	 *
	 * @param authorId the author id
	 * @param expiryDate the expiry date
	 * @throws DAOException the DAO exception
	 */
	public void updateAuthorExpiredById(Long authorId, Timestamp expiryDate) throws DAOException;
	
	/**
	 * Select by name.
	 *
	 * @param authorName the author name
	 * @return the author
	 * @throws DAOException the DAO exception
	 */
	public Author selectByName(String authorName) throws DAOException;
	
	/**
	 * Select existing authors by names.
	 *
	 * @param authorNames the author names
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<Author> selectExistingAuthorsByNames(Collection<String> authorNames) throws DAOException;
	
	/**
	 * Insert all authors.
	 *
	 * @param authors the authors
	 * @throws DAOException the DAO exception
	 */
	public void insertAllAuthors(Collection<Author> authors) throws DAOException;
	
	/**
	 * Insert authors to news.
	 *
	 * @param newsId the news id
	 * @param authorIds the author ids
	 * @throws DAOException the DAO exception
	 */
	public void insertAuthorsToNews(Long newsId, Collection<Long> authorIds) throws DAOException;
	
	/**
	 * Select authors by news id.
	 *
	 * @param newsId the news id
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<Author> selectAuthorsByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Select authors by each news id.
	 *
	 * @param newsIds the news ids
	 * @return the map
	 * @throws DAOException the DAO exception
	 */
	public Map<Long, Collection<Author>> selectAuthorsByEachNewsId(Collection<Long> newsIds) throws DAOException;
	
	/**
	 * Delete author from news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws DAOException the DAO exception
	 */
	public void deleteAuthorFromNews(Long newsId, Long authorId) throws DAOException;
	
	/**
	 * Delete all authors from news.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception
	 */
	public void deleteAllAuthorsFromNews(Long newsId) throws DAOException;

}
