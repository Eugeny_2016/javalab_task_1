package by.epam.newsmng.dao;

// TODO: Auto-generated Javadoc
/**
 * The Class DAOException.
 */
public class DAOException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Instantiates a new DAO exception.
	 *
	 * @param msg the msg
	 */
	public DAOException(String msg) {
		super(msg);
	}
	
	/**
	 * Instantiates a new DAO exception.
	 *
	 * @param msg the msg
	 * @param ex the ex
	 */
	public DAOException(String msg, Exception ex) {
		super(msg, ex);
	}
	
}
