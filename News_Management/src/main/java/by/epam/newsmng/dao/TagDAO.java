package by.epam.newsmng.dao;

import java.util.Collection;
import java.util.Map;

import by.epam.newsmng.domain.Tag;

// TODO: Auto-generated Javadoc
/**
 * The Interface TagDAO.
 */
public interface TagDAO extends CrudDAO<Tag> {
	
	/**
	 * Select existing tags by names.
	 *
	 * @param tagNames the tag names
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<Tag> selectExistingTagsByNames(Collection<String> tagNames) throws DAOException;
	
	/**
	 * Insert all tags.
	 *
	 * @param tags the tags
	 * @throws DAOException the DAO exception
	 */
	public void insertAllTags(Collection<Tag> tags) throws DAOException;
		
	/**
	 * Insert tags to news.
	 *
	 * @param newsId the news id
	 * @param tagIds the tag ids
	 * @throws DAOException the DAO exception
	 */
	public void insertTagsToNews(Long newsId, Collection<Long> tagIds) throws DAOException;
	
	/**
	 * Select all tags by news id.
	 *
	 * @param newsId the news id
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<Tag> selectAllTagsByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Select tags by each news id.
	 *
	 * @param newsIds the news ids
	 * @return the map
	 * @throws DAOException the DAO exception
	 */
	public Map<Long, Collection<Tag>> selectTagsByEachNewsId(Collection<Long> newsIds) throws DAOException;
	
	/**
	 * Delete tag from news.
	 *
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws DAOException the DAO exception
	 */
	public void deleteTagFromNews(Long newsId, Long tagId) throws DAOException;

	/**
	 * Delete all tags from news.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception
	 */
	public void deleteAllTagsFromNews(Long newsId) throws DAOException;
}
