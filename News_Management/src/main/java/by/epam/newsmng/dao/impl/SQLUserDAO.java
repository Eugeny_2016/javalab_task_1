package by.epam.newsmng.dao.impl;

import static by.epam.newsmng.constant.DBColumnName.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.UserDAO;
import by.epam.newsmng.domain.User;

// TODO: Auto-generated Javadoc
/**
 * The Class SQLUserDAO.
 */
@Repository
public class SQLUserDAO implements UserDAO {
	
	/** The Constant USER_SQL_INSERT. */
	private static final String USER_SQL_INSERT = "INSERT INTO USERS (ROLE_ROLE_ID, USER_NAME, LOGIN, PASSWORD) VALUES(?,?,?,?)";
	
	/** The Constant USER_BY_ID_SQL_SELECT. */
	private static final String USER_BY_ID_SQL_SELECT = "SELECT USER_ID, ROLE_ROLE_ID, USER_NAME, LOGIN, PASSWORD "
													  + "FROM USERS "
													  + "WHERE USER_ID = ?";
	
	/** The Constant USER_ALL_SQL_SELECT. */
	private static final String USER_ALL_SQL_SELECT = "SELECT USER_ID, ROLE_ROLE_ID, USER_NAME, LOGIN, PASSWORD "
												    + "FROM USERS";
	
	/** The Constant USER_INFORMATION_SQL_UPDATE. */
	private static final String USER_INFORMATION_SQL_UPDATE = "UPDATE USERS "
													 		+ "SET ROLE_ROLE_ID = ?, USER_NAME = ?, LOGIN = ?, PASSWORD = ? "
													 		+ "WHERE USER_ID = ?";
	
	/** The Constant USER_BY_ID_SQL_DELETE. */
	private static final String USER_BY_ID_SQL_DELETE = "DELETE FROM USERS "
													  + "WHERE USER_ID = ?";
	
	/** The Constant USER_BY_LOGIN_SQL_SELECT. */
	private static final String USER_BY_LOGIN_SQL_SELECT = "SELECT USER_ID, ROLE_ROLE_ID, USER_NAME, LOGIN, PASSWORD "
			  											 + "FROM USERS "
			  											 + "WHERE LOGIN = ? ";
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * Sets the data source.
	 *
	 * @param dataSource the new data source
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#insert(java.lang.Object)
	 */
	@Override
	public Long insert(User user) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long tagId = null;
		String[] columnsToReturn = {USER_ID};
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(USER_SQL_INSERT, columnsToReturn);
			ps.setLong(1, user.getRoleId());
			ps.setString(2, user.getUserName());
			ps.setString(3, user.getLogin());
			ps.setString(4, user.getPassword());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			
			while (rs.next()) {
				tagId = rs.getLong(1);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting a user. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tagId;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectById(java.lang.Long)
	 */
	@Override
	public User selectById(Long userId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = new User();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(USER_BY_ID_SQL_SELECT);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				user.setUserId(rs.getLong(USER_ID));
				user.setRoleId(rs.getLong(ROLE_ROLE_ID));
				user.setUserName(rs.getString(USER_NAME));
				user.setLogin(rs.getString(USER_LOGIN));
				user.setPassword(rs.getString(USER_PASSWORD));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting a user by id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return user;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectAll()
	 */
	@Override
	public Collection<User> selectAll() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<User> listOfUsers = new ArrayList<User>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(USER_ALL_SQL_SELECT);
			
			while (rs.next()) {
				User user = new User();
				user.setUserId(rs.getLong(USER_ID));
				user.setRoleId(rs.getLong(ROLE_ROLE_ID));
				user.setUserName(rs.getString(USER_NAME));
				user.setLogin(rs.getString(USER_LOGIN));
				user.setPassword(rs.getString(USER_PASSWORD));
				listOfUsers.add(user);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of users. ", ex);
		} finally {
			closeStatement(st);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfUsers;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#update(java.lang.Object)
	 */
	@Override
	public void update(User user) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(USER_INFORMATION_SQL_UPDATE);
			ps.setLong(1, user.getRoleId());
			ps.setString(2, user.getUserName());
			ps.setString(3, user.getLogin());
			ps.setString(4, user.getPassword());
			ps.setLong(5, user.getUserId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while updating the user's information. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long userId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(USER_BY_ID_SQL_DELETE);
			ps.setLong(1, userId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting the user. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.UserDAO#selectByLogin(java.lang.String)
	 */
	@Override
	public User selectByLogin(String login) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = new User();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(USER_BY_LOGIN_SQL_SELECT);
			ps.setString(1, login);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				user.setUserId(rs.getLong(USER_ID));
				user.setRoleId(rs.getLong(ROLE_ROLE_ID));
				user.setUserName(rs.getString(USER_NAME));
				user.setLogin(rs.getString(USER_LOGIN));
				user.setPassword(rs.getString(USER_PASSWORD));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting the user by login. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return user;
	}

}
