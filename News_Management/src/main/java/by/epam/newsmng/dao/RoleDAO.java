package by.epam.newsmng.dao;

import by.epam.newsmng.domain.Role;

// TODO: Auto-generated Javadoc
/**
 * The Interface RoleDAO.
 */
public interface RoleDAO extends CrudDAO<Role> {
	
	/**
	 * Select by name.
	 *
	 * @param roleName the role name
	 * @return the role
	 * @throws DAOException the DAO exception
	 */
	public Role selectByName(String roleName) throws DAOException;

}
