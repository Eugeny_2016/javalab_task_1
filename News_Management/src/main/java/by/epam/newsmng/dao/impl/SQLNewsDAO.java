package by.epam.newsmng.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.NewsDAO;
import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.SearchCriteria;

import static by.epam.newsmng.constant.DBColumnName.*;

// TODO: Auto-generated Javadoc
/**
 * The Class SQLNewsDAO.
 */
@Repository
public class SQLNewsDAO implements NewsDAO {
	
	/** The Constant NEWS_SQL_INSERT. */
	private static final String NEWS_SQL_INSERT = "INSERT INTO NEWS (TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES(?,?,?,?,?)";
	
	/** The Constant NEWS_BY_ID_SQL_SELECT. */
	private static final String NEWS_BY_ID_SQL_SELECT = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
													  + "FROM NEWS "
													  + "WHERE NEWS_ID = ?";
	
	/** The Constant NEWS_ALL_SQL_SELECT. */
	private static final String NEWS_ALL_SQL_SELECT = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
												    + "FROM NEWS";
	
	/** The Constant NEWS_TEXT_INFORMATION_SQL_UPDATE. */
	private static final String NEWS_TEXT_INFORMATION_SQL_UPDATE = "UPDATE NEWS "
													   			 + "SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ? "
													   			 + "WHERE NEWS_ID = ?";
	
	/** The Constant NEWS_BY_ID_SQL_DELETE. */
	private static final String NEWS_BY_ID_SQL_DELETE = "DELETE FROM NEWS "
													  + "WHERE NEWS_ID = ?";
	
	/** The Constant NEWS_BY_TITLE_SQL_SELECT. */
	private static final String NEWS_BY_TITLE_SQL_SELECT = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
														 + "FROM NEWS "
														 + "WHERE TITLE = ?";
	
	/** The Constant SEVERAL_NEWS_SQL_SELECT_PART1. */
	private static final String SEVERAL_NEWS_SQL_SELECT_PART1 = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
			 												  + "FROM NEWS "
			 												  + "WHERE NEWS_ID IN (";
	
	/** The Constant SEVERAL_NEWS_SQL_SELECT_PART2. */
	private static final String SEVERAL_NEWS_SQL_SELECT_PART2 = ") ORDER BY NEWS_ID ASC";
	
	/** The Constant NEWS_N_PIECES_SQL_SELECT. */
	private static final String NEWS_N_PIECES_SQL_SELECT = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
														 + "FROM NEWS "
														 + "WHERE NEWS_ID > ? AND NEWS_ID <= ? "
														 + "ORDER BY NEWS_ID DESC";
	
	/** The Constant NEWS_BY_TITLE_SQL_DELETE. */
	private static final String NEWS_BY_TITLE_SQL_DELETE = "DELETE FROM NEWS "
			  											 + "WHERE TITLE = ?";
	
	/** The Constant NEWS_COUNT_ALL_SQL_SELECT. */
	private static final String NEWS_COUNT_ALL_SQL_SELECT= "SELECT COUNT(*) FROM NEWS";
	
	/** The Constant NEWS_MAX_NEWS_ID_SQL_SELECT. */
	@SuppressWarnings("unused")
	private static final String NEWS_MAX_NEWS_ID_SQL_SELECT= "SELECT MAX(NEWS_ID) FROM NEWS";
	
	/** The Constant NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART1. */
	private static final String NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART1 = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, SUM(AMOUNT) "
																		 + "FROM ( "
																		 + "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, COUNT(NEWS_ID) AS AMOUNT "
																		 + "FROM NEWS "
																		 + "INNER JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_NEWS_ID) " 
																		 + "WHERE NEWS_AUTHOR.AUTHOR_AUTHOR_ID IN (";
	
	/** The Constant NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART2. */
	private static final String NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART2 = ") GROUP BY NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
																		 + "UNION ALL "
																		 + "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, COUNT(NEWS_ID) AS AMOUNT " 
																		 + "FROM NEWS "
																		 + "INNER JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_NEWS_ID) " 
																		 + "WHERE NEWS_TAG.TAG_TAG_ID IN (";
	
	/** The Constant NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART3. */
	private static final String NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART3 = ") GROUP BY NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
																		 + ") "
																		 + "GROUP BY NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE "
																		 + "HAVING SUM(AMOUNT) = ?";
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * Sets the data source.
	 *
	 * @param dataSource the new data source
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#insert(java.lang.Object)
	 */
	@Override
	public Long insert(News news) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long newsId = null;
		String[] columnsToReturn = {NEWS_ID};
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(NEWS_SQL_INSERT, columnsToReturn);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, news.getCreationDate());
			ps.setDate(5, news.getModificationDate());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			
			while (rs.next()) {
				newsId = rs.getLong(1);
			}			
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsId;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectById(java.lang.Long)
	 */
	@Override
	public News selectById(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		News news = new News();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(NEWS_BY_ID_SQL_SELECT);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				news.setNewsId(rs.getLong(NEWS_ID));
				news.setTitle(rs.getString(NEWS_TITLE));
				news.setShortText(rs.getString(NEWS_SHORT_TEXT));
				news.setFullText(rs.getString(NEWS_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting news by id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return news;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectAll()
	 */
	@Override
	public Collection<News> selectAll() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<News> listOfNews = new ArrayList<News>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(NEWS_ALL_SQL_SELECT);
			
			while (rs.next()) {
				News news = new News();
				news.setNewsId(rs.getLong(NEWS_ID));
				news.setTitle(rs.getString(NEWS_TITLE));
				news.setShortText(rs.getString(NEWS_SHORT_TEXT));
				news.setFullText(rs.getString(NEWS_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
				listOfNews.add(news);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting all news. ", ex);
		} finally {
			closeStatement(st);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfNews;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#update(java.lang.Object)
	 */
	@Override
	public void update(News news) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(NEWS_TEXT_INFORMATION_SQL_UPDATE);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setLong(4, news.getNewsId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while updating news text information. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(NEWS_BY_ID_SQL_DELETE);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting news by id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.NewsDAO#selectByTitle(java.lang.String)
	 */
	@Override
	public News selectByTitle(String newsTitle) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		News news = new News();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(NEWS_BY_TITLE_SQL_SELECT);
			ps.setString(1, newsTitle);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				news.setNewsId(rs.getLong(NEWS_ID));
				news.setTitle(rs.getString(NEWS_TITLE));
				news.setShortText(rs.getString(NEWS_SHORT_TEXT));
				news.setFullText(rs.getString(NEWS_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting news by title. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return news;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.NewsDAO#selectSeveralNews(java.util.Collection)
	 */
	@Override
	public Collection<News> selectSeveralNews(Collection<Long> newsIds) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder stringOfNewsIds = new StringBuilder();
		List<News> listOfNews = new ArrayList<News>();
		
		stringOfNewsIds.append(SEVERAL_NEWS_SQL_SELECT_PART1);
		for(Long newsId : newsIds) {
			stringOfNewsIds.append(newsId);
			stringOfNewsIds.append(",");
		}
		stringOfNewsIds.deleteCharAt(stringOfNewsIds.length()-1);
		stringOfNewsIds.append(SEVERAL_NEWS_SQL_SELECT_PART2);
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(stringOfNewsIds.toString());			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				News news = new News();
				news.setNewsId(rs.getLong(NEWS_ID));
				news.setTitle(rs.getString(NEWS_TITLE));
				news.setShortText(rs.getString(NEWS_SHORT_TEXT));
				news.setFullText(rs.getString(NEWS_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
				listOfNews.add(news);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting several news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfNews;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.NewsDAO#selectNextN_News(long, long)
	 */
	@Override
	public Collection<News> selectNextN_News(long maxValue, long minValue) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<News> listOfNews = new ArrayList<News>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(NEWS_N_PIECES_SQL_SELECT);
			ps.setLong(1, minValue);
			ps.setLong(2, maxValue);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				News news = new News();
				news.setNewsId(rs.getLong(NEWS_ID));
				news.setTitle(rs.getString(NEWS_TITLE));
				news.setShortText(rs.getString(NEWS_SHORT_TEXT));
				news.setFullText(rs.getString(NEWS_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
				listOfNews.add(news);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting next n-pieces of news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfNews;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.NewsDAO#selectBySearchCriteria(by.epam.newsmng.domain.SearchCriteria)
	 */
	@Override
	public Collection<News> selectBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<News> listOfNews = new ArrayList<News>();		
		int amountOfAutorsAndTags = searchCriteria.getListOfAuthorIds().size() + searchCriteria.getListOfTagIds().size();
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(queryBuilder(searchCriteria));
			ps.setInt(1, amountOfAutorsAndTags);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				News news = new News();
				news.setNewsId(rs.getLong(NEWS_ID));
				news.setTitle(rs.getString(NEWS_TITLE));
				news.setShortText(rs.getString(NEWS_SHORT_TEXT));
				news.setFullText(rs.getString(NEWS_FULL_TEXT));
				news.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
				news.setModificationDate(rs.getDate(NEWS_MODIFICATION_DATE));
				listOfNews.add(news);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting news by search criteria. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfNews;
	}	

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.NewsDAO#deleteByTitle(java.lang.String)
	 */
	@Override
	public void deleteByTitle(String newsTitle) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
						
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(NEWS_BY_TITLE_SQL_DELETE);
			ps.setString(1, newsTitle);
			ps.executeUpdate();
			
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting news by title. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.NewsDAO#selectAmountOfNews()
	 */
	@Override
	public long selectAmountOfNews() throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		long amountOfNews = 0L;
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(NEWS_COUNT_ALL_SQL_SELECT);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				amountOfNews = rs.getLong(1);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while counting all news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return amountOfNews;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.NewsDAO#selectAmountOfNewsBySearchCriteria(by.epam.newsmng.domain.SearchCriteria)
	 */
	@Override
	public long selectAmountOfNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int amountOfAutorsAndTags = searchCriteria.getListOfAuthorIds().size() + searchCriteria.getListOfTagIds().size();
		long amountOfNews = 0L;
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(queryBuilder(searchCriteria));
			ps.setInt(1, amountOfAutorsAndTags);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				amountOfNews++;
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while counting news by search criteria. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return amountOfNews;
	}
	
	/**
	 * Query builder.
	 *
	 * @param searchCriteria the search criteria
	 * @return the string
	 */
	private String queryBuilder(SearchCriteria searchCriteria) {
		StringBuilder stringOfAuthorIds = new StringBuilder();
		StringBuilder stringOfTagIds = new StringBuilder();
		StringBuilder finalQuery = new StringBuilder();
		List<Long> listOfAuthorIds = searchCriteria.getListOfAuthorIds();
		List<Long> listOfTagIds = searchCriteria.getListOfTagIds();
		
		for (Long authorId : listOfAuthorIds) {
			stringOfAuthorIds.append(authorId);
			stringOfAuthorIds.append(',');
		}
		stringOfAuthorIds.deleteCharAt(stringOfAuthorIds.length()-1);
		
		for (Long tagId : listOfTagIds) {
			stringOfTagIds.append(tagId);
			stringOfTagIds.append(',');
		}
		stringOfTagIds.deleteCharAt(stringOfTagIds.length()-1);
		
		finalQuery.append(NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART1);
		finalQuery.append(stringOfAuthorIds);
		finalQuery.append(NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART2);
		finalQuery.append(stringOfTagIds);
		finalQuery.append(NEWS_BY_SEARCH_CRITERIA_SQL_SELECT_PART3);
		
		return finalQuery.toString();
		
	}
}
