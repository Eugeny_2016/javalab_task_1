package by.epam.newsmng.dao;

import java.util.Collection;
import java.util.Map;

import by.epam.newsmng.domain.Comment;

// TODO: Auto-generated Javadoc
/**
 * The Interface CommentDAO.
 */
public interface CommentDAO extends CrudDAO<Comment> {
	
	/**
	 * Select all comments by news id.
	 *
	 * @param newsId the news id
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<Comment> selectAllCommentsByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Update comment with censuring.
	 *
	 * @param commentId the comment id
	 * @param messageWithReason the message with reason
	 * @throws DAOException the DAO exception
	 */
	public void updateCommentWithCensuring(Long commentId, String messageWithReason) throws DAOException;
	
	/**
	 * Select comments by each news id.
	 *
	 * @param newsIds the news ids
	 * @return the map
	 * @throws DAOException the DAO exception
	 */
	public Map<Long, Collection<Comment>> selectCommentsByEachNewsId(Collection<Long> newsIds) throws DAOException;
	
	/**
	 * Delete comment from news.
	 *
	 * @param newsId the news id
	 * @param commentId the comment id
	 * @throws DAOException the DAO exception
	 */
	public void deleteCommentFromNews(Long newsId, Long commentId) throws DAOException;
	
	/**
	 * Delete all comments from news.
	 *
	 * @param newsId the news id
	 * @throws DAOException the DAO exception
	 */
	public void deleteAllCommentsFromNews(Long newsId) throws DAOException;
	
	/**
	 * Select n most commented news id.
	 *
	 * @param amountOfNewsId the amount of news id
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<Long> selectNMostCommentedNewsId(int amountOfNewsId) throws DAOException;
	
}
