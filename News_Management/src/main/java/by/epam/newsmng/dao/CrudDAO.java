package by.epam.newsmng.dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

// TODO: Auto-generated Javadoc
/**
 * The Interface CrudDAO.
 *
 * @param <T> the generic type
 */
public interface CrudDAO <T> {
	
	/**
	 * Insert.
	 *
	 * @param obj the obj
	 * @return the long
	 * @throws DAOException the DAO exception
	 */
	public Long insert(T obj) throws DAOException;
	
	/**
	 * Select by id.
	 *
	 * @param id the id
	 * @return the t
	 * @throws DAOException the DAO exception
	 */
	public T selectById(Long id) throws DAOException;
	
	/**
	 * Select all.
	 *
	 * @return the collection
	 * @throws DAOException the DAO exception
	 */
	public Collection<T> selectAll() throws DAOException;
	
	/**
	 * Update.
	 *
	 * @param obj the obj
	 * @throws DAOException the DAO exception
	 */
	public void update(T obj) throws DAOException;
	
	/**
	 * Delete by id.
	 *
	 * @param id the id
	 * @throws DAOException the DAO exception
	 */
	public void deleteById(Long id) throws DAOException;
	
	/**
	 * Close statement.
	 *
	 * @param st the st
	 * @throws DAOException the DAO exception
	 */
	public default void closeStatement(Statement st) throws DAOException {
		try {
			if (st != null) {
				st.close();
			} 
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised in closing a statement");
		}
	}
	
	/*public default void releaseConnection(Connection con) throws DAOException {
		try {
			if(con != null) {
				con.close();
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised in releasing a connection");
		}
	}*/
	
}
