package by.epam.newsmng.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import by.epam.newsmng.dao.AuthorDAO;
import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.domain.Author;

import static by.epam.newsmng.constant.DBColumnName.*;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

// TODO: Auto-generated Javadoc
/**
 * The Class SQLAuthorDAO.
 */
@Repository
public class SQLAuthorDAO implements AuthorDAO {
	
	/** The Constant AUTHOR_SQL_INSERT. */
	private static final String AUTHOR_SQL_INSERT = "INSERT INTO AUTHOR (AUTHOR_NAME, EXPIRED) VALUES(?,?)";
	
	/** The Constant AUTHOR_BY_ID_SQL_SELECT. */
	private static final String AUTHOR_BY_ID_SQL_SELECT = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED "
														+ "FROM AUTHOR "
														+ "WHERE AUTHOR_ID = ?";
	
	/** The Constant AUTHOR_ALL_SQL_SELECT. */
	private static final String AUTHOR_ALL_SQL_SELECT = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED "
													  + "FROM AUTHOR";
	
	/** The Constant AUTHOR_NAME_SQL_UPDATE. */
	private static final String AUTHOR_NAME_SQL_UPDATE = "UPDATE AUTHOR "
													   + "SET AUTHOR_NAME = ? "
													   + "WHERE AUTHOR_ID = ?";
	
	/** The Constant AUTHOR_EXPIRED_SQL_UPDATE. */
	private static final String AUTHOR_EXPIRED_SQL_UPDATE = "UPDATE AUTHOR "
														  + "SET EXPIRED = ? "
													      + "WHERE AUTHOR_ID = ?";
	
	/** The Constant AUTHOR_BY_ID_SQL_DELETE. */
	private static final String AUTHOR_BY_ID_SQL_DELETE = "DELETE FROM AUTHOR "
														+ "WHERE AUTHOR_ID = ?";
	
	/** The Constant AUTHOR_BY_NAME_SQL_SELECT. */
	private static final String AUTHOR_BY_NAME_SQL_SELECT = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED "
														  + "FROM AUTHOR "
														  + "WHERE AUTHOR_NAME = ?";
	
	/** The Constant AUTHOR_ALL_EXISTING_BY_NAME_SQL_SELECT_PART1. */
	private static final String AUTHOR_ALL_EXISTING_BY_NAME_SQL_SELECT_PART1 = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED "
																			 + "FROM AUTHOR "
																			 + "WHERE AUTHOR_NAME IN (";
	
	/** The Constant AUTHOR_ALL_EXISTING_BY_NAME_SQL_SELECT_PART2. */
	private static final String AUTHOR_ALL_EXISTING_BY_NAME_SQL_SELECT_PART2 = ") ORDER BY AUTHOR_ID";

	/** The Constant AUTHOR_NEWS_SQL_INSERT. */
	private static final String AUTHOR_NEWS_SQL_INSERT ="INSERT INTO NEWS_AUTHOR (NEWS_NEWS_ID, AUTHOR_AUTHOR_ID) VALUES(?,?)";
	
	/** The Constant AUTHOR_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1. */
	private static final String AUTHOR_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1 = "SELECT NEWS_NEWS_ID, AUTHOR_ID, AUTHOR_NAME, EXPIRED "
																 		 	   + "FROM NEWS_AUTHOR INNER JOIN AUTHOR "
																 		 	   + "ON NEWS_AUTHOR.AUTHOR_AUTHOR_ID = AUTHOR.AUTHOR_ID "
																 		 	   + "WHERE NEWS_NEWS_ID IN (";
																 		 	   
	/** The Constant AUTHOR_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2. */
	private static final String AUTHOR_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2 = ") ORDER BY NEWS_NEWS_ID ASC, AUTHOR_ID ASC";
	
	/** The Constant AUTHOR_ALL_BY_NEWS_ID_SQL_SELECT. */
	private static final String AUTHOR_ALL_BY_NEWS_ID_SQL_SELECT = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED "
			 													 + "FROM AUTHOR INNER JOIN NEWS_AUTHOR "
			 													 + "ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_AUTHOR_ID "
			 													 + "WHERE NEWS_NEWS_ID = ? "
			 													 + "ORDER BY AUTHOR_ID";
	
	/** The Constant AUTHOR_BY_ID_FROM_NEWS_DELETE_SQL. */
	private static final String AUTHOR_BY_ID_FROM_NEWS_DELETE_SQL= "DELETE FROM NEWS_AUTHOR "
																 + "WHERE NEWS_AUTHOR.NEWS_NEWS_ID = ? AND NEWS_AUTHOR.AUTHOR_AUTHOR_ID = ?";

	/** The Constant AUTHOR_ALL_FROM_NEWS_DELETE_SQL. */
	private static final String AUTHOR_ALL_FROM_NEWS_DELETE_SQL = "DELETE FROM NEWS_AUTHOR "
			 													+ "WHERE NEWS_AUTHOR.NEWS_NEWS_ID = ?";
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * Sets the data source.
	 *
	 * @param dataSource the new data source
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#insert(java.lang.Object)
	 */
	@Override
	public Long insert(Author author) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long authorId = null;
		String[] columnsToReturn = {AUTHOR_ID};
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_SQL_INSERT, columnsToReturn);
			ps.setString(1, author.getAuthorName());
			ps.setTimestamp(2, author.getExpiryDate());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			
			while (rs.next()) {
				authorId = rs.getLong(1);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting an author. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authorId;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectById(java.lang.Long)
	 */
	@Override
	public Author selectById(Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = new Author();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_BY_ID_SQL_SELECT);
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				author.setExpiryDate(rs.getTimestamp(AUTHOR_EXPIRED));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting an author by id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return author;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectAll()
	 */
	@Override
	public Collection<Author> selectAll() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<Author> listOfAuthors = new ArrayList<Author>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(AUTHOR_ALL_SQL_SELECT);
			
			while (rs.next()) {
				Author author = new Author();
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				author.setExpiryDate(rs.getTimestamp(AUTHOR_EXPIRED));
				listOfAuthors.add(author);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of authors. ", ex);
		} finally {
			closeStatement(st);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfAuthors;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Author author) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_NAME_SQL_UPDATE);
			ps.setString(1, author.getAuthorName());
			ps.setLong(2, author.getAuthorId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while updating author's name. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_BY_ID_SQL_DELETE);
			ps.setLong(1, authorId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting an author. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#updateAuthorExpiredById(java.lang.Long, java.sql.Timestamp)
	 */
	@Override
	public void updateAuthorExpiredById(Long authorId, Timestamp expiryDate) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_EXPIRED_SQL_UPDATE);
			ps.setTimestamp(1, expiryDate);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while updating author's expiry date. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#selectByName(java.lang.String)
	 */
	@Override
	public Author selectByName(String authorName) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = new Author();
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_BY_NAME_SQL_SELECT);
			ps.setString(1, authorName);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				author.setExpiryDate(rs.getTimestamp(AUTHOR_EXPIRED));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting an author by name. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return author;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#selectExistingAuthorsByNames(java.util.Collection)
	 */
	@Override
	public Collection<Author> selectExistingAuthorsByNames(Collection<String> authorNames) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder inRange = new StringBuilder();
		Collection<Author> listOfExistingAuthors = new ArrayList<Author>();
		
		inRange.append(AUTHOR_ALL_EXISTING_BY_NAME_SQL_SELECT_PART1);
		for (String str : authorNames) {
			inRange.append("'");
			inRange.append(str);
			inRange.append("'");
			inRange.append(",");
		}
		inRange.deleteCharAt(inRange.length()-1);
		inRange.append(AUTHOR_ALL_EXISTING_BY_NAME_SQL_SELECT_PART2);
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(inRange.toString());
			rs = ps.executeQuery();
			
			while(rs.next()) {
				Author author = new Author();
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				author.setExpiryDate(rs.getTimestamp(AUTHOR_EXPIRED));
				listOfExistingAuthors.add(author);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while getting ids of the existing authors", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfExistingAuthors;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#insertAllAuthors(java.util.Collection)
	 */
	@Override
	public void insertAllAuthors(Collection<Author> authors) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_SQL_INSERT);
			for (Author author : authors) {
				ps.setString(1, author.getAuthorName());
				ps.setTimestamp(2, author.getExpiryDate());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting several authors. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#insertAuthorsToNews(java.lang.Long, java.util.Collection)
	 */
	@Override
	public void insertAuthorsToNews(Long newsId, Collection<Long> authorIds) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_NEWS_SQL_INSERT);
			for(Long authorId : authorIds) {
				ps.setLong(1, newsId);
				ps.setLong(2, authorId);
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting author list to news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}	
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#selectAuthorsByNewsId(java.lang.Long)
	 */
	@Override
	public Collection<Author> selectAuthorsByNewsId(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Author> listOfAuthors = new ArrayList<Author>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_ALL_BY_NEWS_ID_SQL_SELECT);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Author author = new Author();
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				author.setExpiryDate(rs.getTimestamp(AUTHOR_EXPIRED));
				listOfAuthors.add(author);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of authors by news id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfAuthors;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#selectAuthorsByEachNewsId(java.util.Collection)
	 */
	@Override
	public Map<Long, Collection<Author>> selectAuthorsByEachNewsId(Collection<Long> newsIds) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder inRange = new StringBuilder();
		Collection<Author> authorsOfCurrentNews = null;
		Map<Long, Collection<Author>> mapOfNewsIdsAndAuthors = new LinkedHashMap<Long, Collection<Author>>();
		
		inRange.append(AUTHOR_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1);
		for (Long id : newsIds) {
			inRange.append(id);
			inRange.append(",");
		}
		inRange.deleteCharAt(inRange.length()-1);
		inRange.append(AUTHOR_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2);
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(inRange.toString());
			rs = ps.executeQuery();
			
			Long prevNewsId = null;
			if (rs.next()) {
				prevNewsId = rs.getLong(NEWS_NEWS_ID);
				Author author = new Author();
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				author.setExpiryDate(rs.getTimestamp(AUTHOR_EXPIRED));
				authorsOfCurrentNews = new ArrayList<Author>();
				authorsOfCurrentNews.add(author);
			}
			
			while (rs.next()) {
				Long newsId = rs.getLong(NEWS_NEWS_ID);
				if (newsId.equals(prevNewsId)) {
					Author author = new Author();
					author.setAuthorId(rs.getLong(AUTHOR_ID));
					author.setAuthorName(rs.getString(AUTHOR_NAME));
					author.setExpiryDate(rs.getTimestamp(AUTHOR_EXPIRED));
					authorsOfCurrentNews.add(author);
				} else {
					mapOfNewsIdsAndAuthors.put(prevNewsId, authorsOfCurrentNews);
					prevNewsId = rs.getLong(NEWS_NEWS_ID);
					Author author = new Author();
					author.setAuthorId(rs.getLong(AUTHOR_ID));
					author.setAuthorName(rs.getString(AUTHOR_NAME));
					author.setExpiryDate(rs.getTimestamp(AUTHOR_EXPIRED));
					authorsOfCurrentNews = new ArrayList<Author>();
					authorsOfCurrentNews.add(author);
				}
				mapOfNewsIdsAndAuthors.put(prevNewsId, authorsOfCurrentNews);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of authors by each news id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return mapOfNewsIdsAndAuthors;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#deleteAuthorFromNews(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteAuthorFromNews(Long newsId, Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_BY_ID_FROM_NEWS_DELETE_SQL);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting an author from news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.AuthorDAO#deleteAllAuthorsFromNews(java.lang.Long)
	 */
	@Override
	public void deleteAllAuthorsFromNews(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(AUTHOR_ALL_FROM_NEWS_DELETE_SQL);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting all authors from news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
}
