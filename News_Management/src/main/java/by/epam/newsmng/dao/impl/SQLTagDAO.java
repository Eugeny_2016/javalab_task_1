package by.epam.newsmng.dao.impl;

import static by.epam.newsmng.constant.DBColumnName.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.TagDAO;
import by.epam.newsmng.domain.Tag;

// TODO: Auto-generated Javadoc
/**
 * The Class SQLTagDAO.
 */
@Repository
public class SQLTagDAO implements TagDAO {
	
	/** The Constant TAG_SQL_INSERT. */
	private static final String TAG_SQL_INSERT = "INSERT INTO TAG (TAG_NAME) VALUES(?)";
	
	/** The Constant TAG_BY_ID_SQL_SELECT. */
	private static final String TAG_BY_ID_SQL_SELECT = "SELECT TAG_ID, TAG_NAME "
													 + "FROM TAG "
													 + "WHERE TAG_ID = ?";
	
	/** The Constant TAG_ALL_SQL_SELECT. */
	private static final String TAG_ALL_SQL_SELECT = "SELECT TAG_ID, TAG_NAME "
												   + "FROM TAG";
	
	/** The Constant TAG_NAME_SQL_UPDATE. */
	private static final String TAG_NAME_SQL_UPDATE = "UPDATE TAG "
													+ "SET TAG_NAME = ?"
												    + "WHERE TAG_ID = ?";
	
	/** The Constant TAG_BY_ID_SQL_DELETE. */
	private static final String TAG_BY_ID_SQL_DELETE = "DELETE FROM TAG "
													 + "WHERE TAG_ID = ?";
	
	/** The Constant TAG_ALL_EXISTING_BY_NAME_SQL_SELECT_PART1. */
	private static final String TAG_ALL_EXISTING_BY_NAME_SQL_SELECT_PART1 = "SELECT TAG_ID, TAG_NAME "
			 															  + "FROM TAG "
			 															  + "WHERE TAG_NAME IN (";
	
	/** The Constant TAG_ALL_EXISTING_BY_NAME_SQL_SELECT_PART2. */
	private static final String TAG_ALL_EXISTING_BY_NAME_SQL_SELECT_PART2 = ") ORDER BY TAG_ID";
	
	/** The Constant TAG_NEWS_SQL_INSERT. */
	private static final String TAG_NEWS_SQL_INSERT ="INSERT INTO NEWS_TAG (NEWS_NEWS_ID, TAG_TAG_ID) VALUES(?,?)";
	
	/** The Constant TAG_ALL_BY_NEWS_ID_SQL_SELECT. */
	private static final String TAG_ALL_BY_NEWS_ID_SQL_SELECT = "SELECT TAG_ID, TAG_NAME "
															  + "FROM TAG INNER JOIN NEWS_TAG "
															  + "ON TAG.TAG_ID = NEWS_TAG.TAG_TAG_ID "
															  + "WHERE NEWS_NEWS_ID = ? "
															  + "ORDER BY TAG_ID ASC";
	
	/** The Constant TAG_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1. */
	private static final String TAG_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1 = "SELECT NEWS_NEWS_ID, TAG_ID, TAG_NAME "
		 	   																+ "FROM NEWS_TAG INNER JOIN TAG "
		 	   																+ "ON NEWS_TAG.TAG_TAG_ID = TAG.TAG_ID "
		 	   																+ "WHERE NEWS_NEWS_ID IN (";
		 	   
	/** The Constant TAG_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2. */
	private static final String TAG_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2 = ") ORDER BY NEWS_NEWS_ID ASC, TAG_ID ASC";
	
	/** The Constant TAG_BY_ID_FROM_NEWS_DELETE_SQL. */
	private static final String TAG_BY_ID_FROM_NEWS_DELETE_SQL= "DELETE FROM NEWS_TAG "
															  + "WHERE NEWS_TAG.NEWS_NEWS_ID = ? AND NEWS_TAG.TAG_TAG_ID = ?";
	
	/** The Constant TAG_ALL_FROM_NEWS_DELETE_SQL. */
	private static final String TAG_ALL_FROM_NEWS_DELETE_SQL = "DELETE FROM NEWS_TAG "
															 + "WHERE NEWS_TAG.NEWS_NEWS_ID = ?";
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * Sets the data source.
	 *
	 * @param dataSource the new data source
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#insert(java.lang.Object)
	 */
	@Override
	public Long insert(Tag tag) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long tagId = null;
		String[] columnsToReturn = {TAG_ID};
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_SQL_INSERT, columnsToReturn);
			ps.setString(1, tag.getTagName());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			
			while (rs.next()) {
				tagId = rs.getLong(1);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting a tag. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tagId;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectById(java.lang.Long)
	 */
	@Override
	public Tag selectById(Long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Tag tag = new Tag();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_BY_ID_SQL_SELECT);
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				tag.setTagId(rs.getLong(TAG_ID));
				tag.setTagName(rs.getString(TAG_NAME));
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting a tag by id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return tag;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#selectAll()
	 */
	@Override
	public Collection<Tag> selectAll() throws DAOException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<Tag> listOfTags = new ArrayList<Tag>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(TAG_ALL_SQL_SELECT);
			
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setTagId(rs.getLong(TAG_ID));
				tag.setTagName(rs.getString(TAG_NAME));
				listOfTags.add(tag);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of tags. ", ex);
		} finally {
			closeStatement(st);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfTags;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Tag tag) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_NAME_SQL_UPDATE);
			ps.setString(1, tag.getTagName());
			ps.setLong(2, tag.getTagId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while updating tag's name. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.CrudDAO#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_BY_ID_SQL_DELETE);
			ps.setLong(1, tagId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting a tag. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.TagDAO#selectAllTagsByNewsId(java.lang.Long)
	 */
	@Override
	public Collection<Tag> selectAllTagsByNewsId(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Tag> listOfTags = new ArrayList<Tag>();
				
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_ALL_BY_NEWS_ID_SQL_SELECT);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setTagId(rs.getLong(TAG_ID));
				tag.setTagName(rs.getString(TAG_NAME));
				listOfTags.add(tag);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of tags by news id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfTags;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.TagDAO#selectExistingTagsByNames(java.util.Collection)
	 */
	@Override
	public Collection<Tag> selectExistingTagsByNames(Collection<String> tagNames) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder inRange = new StringBuilder();
		Collection<Tag> listOfExistingTags = new ArrayList<Tag>();
		
		inRange.append(TAG_ALL_EXISTING_BY_NAME_SQL_SELECT_PART1);
		for (String str : tagNames) {
			inRange.append("'");
			inRange.append(str);
			inRange.append("'");
			inRange.append(",");
		}
		inRange.deleteCharAt(inRange.length()-1);
		inRange.append(TAG_ALL_EXISTING_BY_NAME_SQL_SELECT_PART2);
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(inRange.toString());
			rs = ps.executeQuery();
			
			while(rs.next()) {
				Tag tag = new Tag();
				tag.setTagId(rs.getLong(TAG_ID));
				tag.setTagName(rs.getString(TAG_NAME));
				listOfExistingTags.add(tag);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while getting ids of the existing tags", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return listOfExistingTags;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.TagDAO#insertAllTags(java.util.Collection)
	 */
	@Override
	public void insertAllTags(Collection<Tag> tags) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_SQL_INSERT);
			for (Tag tag : tags) {
				ps.setString(1, tag.getTagName());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting a tag. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.TagDAO#insertTagsToNews(java.lang.Long, java.util.Collection)
	 */
	@Override
	public void insertTagsToNews(Long newsId, Collection<Long> tagIds) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_NEWS_SQL_INSERT);
			for(Long tagId : tagIds) {
				ps.setLong(1, newsId);
				ps.setLong(2, tagId);
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while inserting tag list to news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.TagDAO#selectTagsByEachNewsId(java.util.Collection)
	 */
	@Override
	public Map<Long, Collection<Tag>> selectTagsByEachNewsId(Collection<Long> newsIds) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder inRange = new StringBuilder();
		Collection<Tag> tagsOfCurrentNews = null;
		Map<Long, Collection<Tag>> mapOfNewsIdsAndTags = new LinkedHashMap<Long, Collection<Tag>>();
		
		inRange.append(TAG_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART1);
		for (Long id : newsIds) {
			inRange.append(id);
			inRange.append(",");
		}
		inRange.deleteCharAt(inRange.length()-1);
		inRange.append(TAG_ALL_BY_SEVERAL_NEWS_ID_SQL_SELECT_PART2);
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(inRange.toString());
			rs = ps.executeQuery();
			
			Long prevNewsId = null;
			if (rs.next()) {
				prevNewsId = rs.getLong(NEWS_NEWS_ID);
				Tag tag = new Tag();
				tag.setTagId(rs.getLong(TAG_ID));
				tag.setTagName(rs.getString(TAG_NAME));
				tagsOfCurrentNews = new ArrayList<Tag>();
				tagsOfCurrentNews.add(tag);
			}
			
			while (rs.next()) {
				Long newsId = rs.getLong(NEWS_NEWS_ID);
				if (newsId.equals(prevNewsId)) {
					Tag tag = new Tag();
					tag.setTagId(rs.getLong(TAG_ID));
					tag.setTagName(rs.getString(TAG_NAME));
					tagsOfCurrentNews.add(tag);
				} else {
					mapOfNewsIdsAndTags.put(prevNewsId, tagsOfCurrentNews);
					prevNewsId = rs.getLong(NEWS_NEWS_ID);
					Tag tag = new Tag();
					tag.setTagId(rs.getLong(TAG_ID));
					tag.setTagName(rs.getString(TAG_NAME));
					tagsOfCurrentNews = new ArrayList<Tag>();
					tagsOfCurrentNews.add(tag);
				}
				mapOfNewsIdsAndTags.put(prevNewsId, tagsOfCurrentNews);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while selecting collection of tags by each news id. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}		
		return mapOfNewsIdsAndTags;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.TagDAO#deleteTagFromNews(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteTagFromNews(Long newsId, Long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_BY_ID_FROM_NEWS_DELETE_SQL);
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting tag from news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.dao.TagDAO#deleteAllTagsFromNews(java.lang.Long)
	 */
	@Override
	public void deleteAllTagsFromNews(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(TAG_ALL_FROM_NEWS_DELETE_SQL);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			throw new DAOException("SQLException arised while deleting all tags from news. ", ex);
		} finally {
			closeStatement(ps);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
}
