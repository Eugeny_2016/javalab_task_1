package by.epam.newsmng.dao;

import by.epam.newsmng.domain.User;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserDAO.
 */
public interface UserDAO extends CrudDAO<User> {
	
	/**
	 * Select by login.
	 *
	 * @param login the login
	 * @return the user
	 * @throws DAOException the DAO exception
	 */
	public User selectByLogin(String login) throws DAOException;
}
