package by.epam.newsmng.domain;

import java.io.Serializable;
import java.util.Collections;
import java.util.Collection;

// TODO: Auto-generated Javadoc
/**
 * The Class AssembledNews.
 */
public class AssembledNews implements Serializable, Comparable<AssembledNews> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The news. */
	private News news;
	
	/** The list of authors. */
	private Collection<Author> listOfAuthors;
	
	/** The list of tags. */
	private Collection<Tag> listOfTags;
	
	/** The list of comments. */
	private Collection<Comment> listOfComments;

	/**
	 * Instantiates a new assembled news.
	 */
	public AssembledNews() {
	}

	/**
	 * Instantiates a new assembled news.
	 *
	 * @param news the news
	 * @param listOfAuthors the list of authors
	 * @param listOfTags the list of tags
	 * @param listOfComments the list of comments
	 */
	public AssembledNews(News news, Collection<Author> listOfAuthors, Collection<Tag> listOfTags, Collection<Comment> listOfComments) {
		this.news = news;
		this.listOfAuthors = listOfAuthors;
		this.listOfTags = listOfTags;
		this.listOfComments = listOfComments;
	}

	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * Gets the authors.
	 *
	 * @return the authors
	 */
	public Collection<Author> getAuthors() {
		return Collections.unmodifiableCollection(listOfAuthors);
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public Collection<Tag> getTags() {
		return Collections.unmodifiableCollection(listOfTags);
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public Collection<Comment> getComments() {
		return Collections.unmodifiableCollection(listOfComments);
	}

	/**
	 * Sets the news.
	 *
	 * @param news the new news
	 */
	public void setNews(News news) {
		this.news = news;
	}

	/**
	 * Sets the authors.
	 *
	 * @param listOfAuthors the new authors
	 */
	public void setAuthors(Collection<Author> listOfAuthors) {
		this.listOfAuthors = listOfAuthors;
	}

	/**
	 * Sets the tags.
	 *
	 * @param listOfTags the new tags
	 */
	public void setTags(Collection<Tag> listOfTags) {
		this.listOfTags = listOfTags;
	}

	/**
	 * Sets the comments.
	 *
	 * @param listOfComments the new comments
	 */
	public void setComments(Collection<Comment> listOfComments) {
		this.listOfComments = listOfComments;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AssembledNews assembledNews) {
		int result = 0;
		
		if (this.news.getCreationDate().after(assembledNews.getNews().getCreationDate())) {
			result = 1;
		} else if (this.news.getCreationDate().before(assembledNews.news.getCreationDate())) {
			result = -1;
		}
		return result;
	}
}
