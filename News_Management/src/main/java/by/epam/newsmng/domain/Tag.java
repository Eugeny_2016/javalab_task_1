package by.epam.newsmng.domain;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class Tag.
 */
public class Tag implements Serializable, Comparable<Tag> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The tag id. */
	private Long tagId;
	
	/** The tag name. */
	private String tagName;
	
	/**
	 * Instantiates a new tag.
	 */
	public Tag() {
	}
	
	/**
	 * Instantiates a new tag.
	 *
	 * @param tagName the tag name
	 */
	public Tag(String tagName) {
		this.tagName = tagName;
	}
	
	/**
	 * Instantiates a new tag.
	 *
	 * @param tagId the tag id
	 * @param tagName the tag name
	 */
	public Tag(Long tagId, String tagName) {
		this.tagId = tagId;
		this.tagName = tagName;
	}

	/**
	 * Gets the tag id.
	 *
	 * @return the tag id
	 */
	public Long getTagId() {
		return tagId;
	}

	/**
	 * Gets the tag name.
	 *
	 * @return the tag name
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * Sets the tag id.
	 *
	 * @param tagId the new tag id
	 */
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	/**
	 * Sets the tag name.
	 *
	 * @param tagName the new tag name
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Tag [tagId=");
		sb.append(tagId);
		sb.append(", tagName=");
		sb.append(tagName);
		sb.append("]");
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Tag other = (Tag) obj;
		if (tagId == null) {
			if (other.tagId != null) {
				return false;
			}
		} else if (!tagId.equals(other.tagId)) {
			return false;
		}
		
		if (tagName == null) {
			if (other.tagName != null) {
				return false;
			}
		} else if (!tagName.equals(other.tagName)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Tag tag) {
		return this.tagName.compareTo(tag.tagName);
	}
}
