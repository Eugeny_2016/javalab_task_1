package by.epam.newsmng.domain;

import java.io.Serializable;
import java.sql.Timestamp;

// TODO: Auto-generated Javadoc
/**
 * The Class Author.
 */
public class Author  implements Serializable, Comparable<Author> {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The author id. */
	private Long authorId;
	
	/** The author name. */
	private String authorName;
	
	/** The expiry date. */
	private Timestamp expiryDate;
	
	/**
	 * Instantiates a new author.
	 */
	public Author() {
	}
	
	/**
	 * Instantiates a new author.
	 *
	 * @param authorName the author name
	 * @param expiryDate the expiry date
	 */
	public Author(String authorName, Timestamp expiryDate) {
		this.authorName = authorName;
		this.expiryDate = expiryDate;
	}
	
	/**
	 * Instantiates a new author.
	 *
	 * @param authorId the author id
	 * @param authorName the author name
	 * @param expiryDate the expiry date
	 */
	public Author(Long authorId, String authorName, Timestamp expiryDate) {
		this.authorId = authorId;
		this.authorName = authorName;
		this.expiryDate = expiryDate;
	}

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Gets the author name.
	 *
	 * @return the author name
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Gets the expiry date.
	 *
	 * @return the expiry date
	 */
	public Timestamp getExpiryDate() {
		return expiryDate;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId the new author id
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Sets the author name.
	 *
	 * @param authorName the new author name
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * Sets the expiry date.
	 *
	 * @param expiryDate the new expiry date
	 */
	public void setExpiryDate(Timestamp expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Author [authorId=");
		sb.append(authorId);
		sb.append(", authorName=");
		sb.append(authorName);
		sb.append(", expiryDate=");
		sb.append(expiryDate);
		sb.append("]");
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expiryDate == null) ? 0 : expiryDate.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Author other = (Author) obj;
		if (authorId == null) {
			if (other.authorId != null) {
				return false;
			}
		} else if (!authorId.equals(other.authorId)) {
			return false;
		}
		
		if (authorName == null) {
			if (other.authorName != null) {
				return false;
			}
		} else if (!authorName.equals(other.authorName)) {
			return false;
		}
		
		if (expiryDate == null) {
			if (other.expiryDate != null) {
				return false;
			}
		} else if (!expiryDate.equals(other.expiryDate)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Author author) {
		return this.authorName.compareTo(author.authorName);
	}
}
