package by.epam.newsmng.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.sql.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class News.
 */
public class News implements Serializable, Comparable<News> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The news id. */
	private Long newsId;
	
	/** The title. */
	private String title;
	
	/** The short text. */
	private String shortText;
	
	/** The full text. */
	private String fullText;
	
	/** The creation date. */
	private Timestamp creationDate;
	
	/** The modification date. */
	private Date modificationDate;

	/**
	 * Instantiates a new news.
	 */
	public News() {
	}
	
	/**
	 * Instantiates a new news.
	 *
	 * @param title the title
	 * @param shortText the short text
	 * @param fullText the full text
	 * @param creationDate the creation date
	 * @param modificationDate the modification date
	 */
	public News(String title, String shortText, String fullText, Timestamp creationDate,
			Date modificationDate) {
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	/**
	 * Instantiates a new news.
	 *
	 * @param newsId the news id
	 * @param title the title
	 * @param shortText the short text
	 * @param fullText the full text
	 * @param creationDate the creation date
	 * @param modificationDate the modification date
	 */
	public News(Long newsId, String title, String shortText, String fullText, Timestamp creationDate,
			Date modificationDate) {
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets the short text.
	 *
	 * @return the short text
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Gets the full text.
	 *
	 * @return the full text
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Timestamp getCreationDate() {
		return creationDate;
	}

	/**
	 * Gets the modification date.
	 *
	 * @return the modification date
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Sets the short text.
	 *
	 * @param shortText the new short text
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Sets the full text.
	 *
	 * @param fullText the new full text
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate the new modification date
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("News [newsId=");
		sb.append(newsId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", shortText=");
		sb.append(shortText);
		sb.append(", creationDate=");
		sb.append(creationDate);
		sb.append(", modificationDate=");
		sb.append(modificationDate);
		sb.append("]");
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		News other = (News) obj;
		
		if (newsId == null) {
			if (other.newsId != null) {
				return false;
			}
		} else if (!newsId.equals(other.newsId)) {
			return false;
		}
		
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(News news) {
		int result = 0;
		
		if (this.creationDate.after(news.creationDate)) {
			result = 1;
		} else if (this.creationDate.before(news.creationDate)) {
			result = -1;
		}
		return result;
	}
}
