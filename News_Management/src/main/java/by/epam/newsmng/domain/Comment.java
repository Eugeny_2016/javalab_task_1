package by.epam.newsmng.domain;

import java.io.Serializable;
import java.sql.Timestamp;

// TODO: Auto-generated Javadoc
/**
 * The Class Comment.
 */
public class Comment implements Serializable, Comparable<Comment> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The comment id. */
	private Long commentId;
	
	/** The news id. */
	private Long newsId;
	
	/** The comment text. */
	private String commentText;
	
	/** The creation date. */
	private Timestamp creationDate;

	/**
	 * Instantiates a new comment.
	 */
	public Comment() {
	}
	
	/**
	 * Instantiates a new comment.
	 *
	 * @param newsId the news id
	 * @param commentText the comment text
	 * @param creationDate the creation date
	 */
	public Comment(Long newsId, String commentText, Timestamp creationDate) {
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}
	
	/**
	 * Instantiates a new comment.
	 *
	 * @param commentId the comment id
	 * @param newsId the news id
	 * @param commentText the comment text
	 * @param creationDate the creation date
	 */
	public Comment(Long commentId, Long newsId, String commentText, Timestamp creationDate) {
		this.commentId = commentId;
		this.newsId = newsId;
		this.commentText = commentText;
		this.creationDate = creationDate;
	}

	/**
	 * Gets the comment id.
	 *
	 * @return the comment id
	 */
	public Long getCommentId() {
		return commentId;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Gets the comment text.
	 *
	 * @return the comment text
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Timestamp getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the comment id.
	 *
	 * @param commentId the new comment id
	 */
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Sets the comment text.
	 *
	 * @param commentText the new comment text
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Comment [commentId=");
		sb.append(commentId);
		sb.append(", newsId=");
		sb.append(newsId);
		sb.append(", commentText=");
		sb.append(commentText);
		sb.append(", creationDate=");
		sb.append(creationDate);
		sb.append("]");
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Comment other = (Comment) obj;
		if (commentId == null) {
			if (other.commentId != null) {
				return false;
			}
		} else if (!commentId.equals(other.commentId)) {
			return false;
		}
		
		if (creationDate == null) {
			if (other.creationDate != null) {
				return false;
			}
		} else if (!creationDate.equals(other.creationDate)) {
			return false;
		}
		
		if (newsId == null) {
			if (other.newsId != null) {
				return false;
			}
		} else if (!newsId.equals(other.newsId)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Comment comment) {
		int result = 0;
		
		if (this.creationDate.after(comment.creationDate)) {
			result = 1;
		} else if(this.creationDate.before(comment.creationDate)) {
			result = -1;
		}		
		return result;
	}
}
