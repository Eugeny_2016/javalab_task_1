package by.epam.newsmng.domain;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class SearchCriteria.
 */
public class SearchCriteria {
	
	/** The list of author ids. */
	private List<Long> listOfAuthorIds;
	
	/** The list of tag ids. */
	private List<Long> listOfTagIds;
	
	/**
	 * Instantiates a new search criteria.
	 */
	public SearchCriteria() {
		listOfAuthorIds = new ArrayList<Long>();
		listOfTagIds = new ArrayList<Long>();
	}
	
	/**
	 * Instantiates a new search criteria.
	 *
	 * @param listOfAuthorIds the list of author ids
	 * @param listOfTagIds the list of tag ids
	 */
	public SearchCriteria(List<Long> listOfAuthorIds, List<Long> listOfTagIds) {
		this.listOfAuthorIds = listOfAuthorIds;
		this.listOfTagIds = listOfTagIds;
	}

	/**
	 * Gets the list of author ids.
	 *
	 * @return the list of author ids
	 */
	public List<Long> getListOfAuthorIds() {
		return listOfAuthorIds;
	}

	/**
	 * Gets the list of tag ids.
	 *
	 * @return the list of tag ids
	 */
	public List<Long> getListOfTagIds() {
		return listOfTagIds;
	}

	/**
	 * Sets the list of author ids.
	 *
	 * @param listOfAuthorIds the new list of author ids
	 */
	public void setListOfAuthorIds(List<Long> listOfAuthorIds) {
		this.listOfAuthorIds = listOfAuthorIds;
	}

	/**
	 * Sets the list of tag ids.
	 *
	 * @param listOfTagIds the new list of tag ids
	 */
	public void setListOfTagIds(List<Long> listOfTagIds) {
		this.listOfTagIds = listOfTagIds;
	}
	
	/**
	 * Adds the author id.
	 *
	 * @param authorId the author id
	 */
	public void addAuthorId(Long authorId) {
		listOfAuthorIds.add(authorId);
	}
	
	/**
	 * Adds the tag id.
	 *
	 * @param tagId the tag id
	 */
	public void addTagId(Long tagId) {
		listOfTagIds.add(tagId);
	}
}
