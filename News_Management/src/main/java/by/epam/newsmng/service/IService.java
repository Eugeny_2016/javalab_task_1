package by.epam.newsmng.service;

import java.util.Collection;

// TODO: Auto-generated Javadoc
/**
 * The Interface IService.
 *
 * @param <T> the generic type
 */
public interface IService<T> {
	
	/**
	 * Adds the.
	 *
	 * @param obj the obj
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public Long add(T obj) throws ServiceException;
	
	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 * @throws ServiceException the service exception
	 */
	public T getById(Long id) throws ServiceException;
	
	/**
	 * Gets the all.
	 *
	 * @return the all
	 * @throws ServiceException the service exception
	 */
	public Collection<T> getAll() throws ServiceException;
	
	/**
	 * Update.
	 *
	 * @param obj the obj
	 * @throws ServiceException the service exception
	 */
	public void update(T obj) throws ServiceException;
	
	/**
	 * Delete by id.
	 *
	 * @param id the id
	 * @throws ServiceException the service exception
	 */
	public void deleteById(Long id) throws ServiceException;
	
}
