package by.epam.newsmng.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.NewsDAO;
import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.SearchCriteria;
import by.epam.newsmng.service.NewsService;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class NewsServiceGroup.
 */
@Service
public class NewsServiceGroup implements NewsService {
	
	/** The news dao. */
	@Autowired
	private NewsDAO newsDAO;
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#add(java.lang.Object)
	 */
	@Override
	public Long add(News news) throws ServiceException {
		Long newsId = null;
		try {
			newsId = newsDAO.insert(news);
		} catch (DAOException ex) {
			throw new ServiceException("Can't add news. ", ex);
		}
		return newsId;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getById(java.lang.Long)
	 */
	@Override
	public News getById(Long newsId) throws ServiceException {
		News news = null;
		try {
			news = newsDAO.selectById(newsId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get news by id. ", ex);
		}
		return news;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getAll()
	 */
	@Override
	public Collection<News> getAll() throws ServiceException {
		Collection<News> news = null;
		try {
			news = newsDAO.selectAll();
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all news. ", ex);
		}
		return news;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException ex) {
			throw new ServiceException("Can't update news. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long newsId) throws ServiceException {
		try {
			newsDAO.deleteById(newsId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete news by id. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.NewsService#getByTitle(java.lang.String)
	 */
	@Override
	public News getByTitle(String newsTitle) throws ServiceException {
		News news = null;
		try {
			news = newsDAO.selectByTitle(newsTitle);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get news by title. ", ex);
		}
		return news;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.NewsService#getSeveralNews(java.util.Collection)
	 */
	@Override
	public Collection<News> getSeveralNews(Collection<Long> newsIds) throws ServiceException {
		Collection<News> news = null;
		try {
			news = newsDAO.selectSeveralNews(newsIds);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get several news. ", ex);
		}
		return news;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.NewsService#getNextN_News(long, long)
	 */
	@Override
	public Collection<News> getNextN_News(long maxValue, long minValue) throws ServiceException {
		Collection<News> news = null;
		try {
			news = newsDAO.selectNextN_News(maxValue, minValue);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get next N news. ", ex);
		}
		return news;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.NewsService#getNewsBySearchCriteria(by.epam.newsmng.domain.SearchCriteria)
	 */
	@Override
	public Collection<News> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		Collection<News> news = null;
		try {
			news = newsDAO.selectBySearchCriteria(searchCriteria);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get next by search criteria. ", ex);
		}
		return news;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.NewsService#deleteByTitle(java.lang.String)
	 */
	@Override
	public void deleteByTitle(String newsTitle) throws ServiceException {
		try {
			newsDAO.deleteByTitle(newsTitle);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete news by search criteria. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.NewsService#countAllNews()
	 */
	@Override
	public long countAllNews() throws ServiceException {
		long amountOfAllNews = 0L;
		try {
			amountOfAllNews = newsDAO.selectAmountOfNews();
		} catch (DAOException ex) {
			throw new ServiceException("Can't count all news. ", ex);
		}
		return amountOfAllNews;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.NewsService#countNewsBySearchCriteria(by.epam.newsmng.domain.SearchCriteria)
	 */
	@Override
	public long countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		long amountOfAllNews = 0L;
		try {
			amountOfAllNews = newsDAO.selectAmountOfNewsBySearchCriteria(searchCriteria);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete news by search criteria. ", ex);
		}
		return amountOfAllNews;
	}
}
