package by.epam.newsmng.service;

import by.epam.newsmng.domain.Role;

// TODO: Auto-generated Javadoc
/**
 * The Interface RoleService.
 */
public interface RoleService extends IService<Role> {
	
	/**
	 * Gets the by name.
	 *
	 * @param roleName the role name
	 * @return the by name
	 * @throws ServiceException the service exception
	 */
	public Role getByName(String roleName) throws ServiceException;
	
}
