package by.epam.newsmng.service.impl;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newsmng.dao.CommentDAO;
import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.domain.Comment;
import by.epam.newsmng.service.CommentService;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class CommentServiceGroup.
 */
@Service
public class CommentServiceGroup implements CommentService {
	
	/** The comment dao. */
	@Autowired
	private CommentDAO commentDAO;
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#add(java.lang.Object)
	 */
	@Override
	public Long add(Comment comment) throws ServiceException {
		Long commentId = null;
		try {
			commentId = commentDAO.insert(comment);
		} catch (DAOException ex) {
			throw new ServiceException("Can't add a comment. ", ex);
		}
		return commentId;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(Comment comment) throws ServiceException {
		try {
			commentDAO.update(comment);
		} catch (DAOException ex) {
			throw new ServiceException("Can't update a comment. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getById(java.lang.Long)
	 */
	@Override
	public Comment getById(Long commentId) throws ServiceException {
		Comment comment = null;
		try {
			comment = commentDAO.selectById(commentId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get a comment. ", ex);
		}
		return comment;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getAll()
	 */
	@Override
	public Collection<Comment> getAll() throws ServiceException {
		Collection<Comment> comments = null;
		try {
			comments = commentDAO.selectAll();
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all comments. ", ex);
		}
		return comments;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long commentId) throws ServiceException {
		try {
			commentDAO.deleteById(commentId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete a comment. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.CommentService#getCommentsByNewsId(java.lang.Long)
	 */
	@Override
	public Collection<Comment> getCommentsByNewsId(Long newsId) throws ServiceException {
		Collection<Comment> comments = null;
		try {
			comments = commentDAO.selectAllCommentsByNewsId(newsId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all comments by news id. ", ex);
		}
		return comments;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.CommentService#censureComment(java.lang.Long, java.lang.String)
	 */
	@Override
	public void censureComment(Long commentId, String messageWithReason) throws ServiceException {
		try {
			commentDAO.updateCommentWithCensuring(commentId, messageWithReason);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get censure comment by news id. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.CommentService#getCommentsByEachNewsId(java.util.Collection)
	 */
	@Override
	public Map<Long, Collection<Comment>> getCommentsByEachNewsId(Collection<Long> newsIds) throws ServiceException {
		Map<Long, Collection<Comment>> mapOfNewsIdsAndCommentlists = null;
		try {
			mapOfNewsIdsAndCommentlists = commentDAO.selectCommentsByEachNewsId(newsIds);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all comments for each news. ", ex);
		}
		return mapOfNewsIdsAndCommentlists;
	}	

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.CommentService#deleteCommentFromNews(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteCommentFromNews(Long newsId, Long commentId) throws ServiceException {
		try {
			commentDAO.deleteCommentFromNews(newsId, commentId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get delete comment from news. ", ex);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.CommentService#deleteCommentsFromNews(java.lang.Long)
	 */
	@Override
	public void deleteCommentsFromNews(Long newsId) throws ServiceException {
		try {
			commentDAO.deleteAllCommentsFromNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get delete all comments from news. ", ex);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.CommentService#getNMostCommentedNewsId(int)
	 */
	@Override
	public Collection<Long> getNMostCommentedNewsId(int amountOfNewsId) throws ServiceException {
		Collection<Long> newsIds = null;
		try {
			newsIds = commentDAO.selectNMostCommentedNewsId(amountOfNewsId);
		} catch(DAOException ex) {
			throw new ServiceException("Can't get a list of most commented news. ", ex);
		}		
		return newsIds;
	}
}
