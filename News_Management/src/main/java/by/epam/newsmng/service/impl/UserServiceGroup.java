package by.epam.newsmng.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.UserDAO;
import by.epam.newsmng.domain.User;
import by.epam.newsmng.service.ServiceException;
import by.epam.newsmng.service.UserService;

// TODO: Auto-generated Javadoc
/**
 * The Class UserServiceGroup.
 */
@Service
public class UserServiceGroup implements UserService {
	
	/** The user dao. */
	@Autowired
	private UserDAO userDAO;
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#add(java.lang.Object)
	 */
	@Override
	public Long add(User obj) throws ServiceException {
		Long userId;
		try {
			userId = userDAO.insert(obj);
		} catch(DAOException ex) {
			throw new ServiceException("Can't add user. " + ex);
		}
		return userId;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getById(java.lang.Long)
	 */
	@Override
	public User getById(Long id) throws ServiceException {
		User user;
		try {
			user = userDAO.selectById(id);
		} catch(DAOException ex) {
			throw new ServiceException("Can't get user by id. " + ex);
		}
		return user;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getAll()
	 */
	@Override
	public Collection<User> getAll() throws ServiceException {
		Collection<User> users;
		try {
			users = userDAO.selectAll();
		} catch(DAOException ex) {
			throw new ServiceException("Can't get all users. " + ex);
		}
		return users;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(User obj) throws ServiceException {
		try {
			userDAO.update(obj);
		} catch(DAOException ex) {
			throw new ServiceException("Can't update user. " + ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long id) throws ServiceException {
		try {
			userDAO.deleteById(id);
		} catch(DAOException ex) {
			throw new ServiceException("Can't delete user by id. " + ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.UserService#getByLogin(java.lang.String)
	 */
	@Override
	public User getByLogin(String login) throws ServiceException {
		User user;
		try {
			user = userDAO.selectByLogin(login);
		} catch(DAOException ex) {
			throw new ServiceException("Can't get user by login. " + ex);
		}
		return user;
	}

}
