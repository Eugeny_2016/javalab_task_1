package by.epam.newsmng.service.impl;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.TagDAO;
import by.epam.newsmng.domain.Tag;
import by.epam.newsmng.service.ServiceException;
import by.epam.newsmng.service.TagService;

// TODO: Auto-generated Javadoc
/**
 * The Class TagServiceGroup.
 */
@Service
public class TagServiceGroup implements TagService {
	
	/** The tag dao. */
	@Autowired
	private TagDAO tagDAO;
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#add(java.lang.Object)
	 */
	@Override
	public Long add(Tag tag) throws ServiceException {
		Long tagId = null;
		try {
			tagId = tagDAO.insert(tag);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get the existing tags. ", ex);
		}
		return tagId;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getById(java.lang.Long)
	 */
	@Override
	public Tag getById(Long tagId) throws ServiceException {
		Tag tag = null;
		try {
			tag = tagDAO.selectById(tagId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get tag by id. ", ex);
		}
		return tag;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getAll()
	 */
	@Override
	public Collection<Tag> getAll() throws ServiceException {
		Collection<Tag> tags = null;
		try {
			tags = tagDAO.selectAll();
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all tags. ", ex);
		}
		return tags;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(Tag tag) throws ServiceException {
		try {
			tagDAO.update(tag);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get the existing tags. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long tagId) throws ServiceException {
		try {
			tagDAO.deleteById(tagId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete tag. ", ex);
		}	
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.TagService#getExistingTagsByNames(java.util.Collection)
	 */
	@Override
	public Collection<Tag> getExistingTagsByNames(Collection<String> tagNames) throws ServiceException {
		Collection<Tag> existingTags = null;
		try {
			existingTags = tagDAO.selectExistingTagsByNames(tagNames);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get the existing tags. ", ex);
		}
		return existingTags;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.TagService#addAllTags(java.util.Collection)
	 */
	@Override
	public void addAllTags(Collection<Tag> tags) throws ServiceException {
		try {
			tagDAO.insertAllTags(tags);
		} catch (DAOException ex) {
			throw new ServiceException("Can't add tags. ", ex);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.TagService#addTagsToNews(java.lang.Long, java.util.Collection)
	 */
	@Override
	public void addTagsToNews(Long newsId, Collection<Long> tagIds) throws ServiceException {
		try {
			tagDAO.insertTagsToNews(newsId, tagIds);
		} catch (DAOException ex) {
			throw new ServiceException("Can't attach tags to news. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.TagService#getTagsByNewsId(java.lang.Long)
	 */
	@Override
	public Collection<Tag> getTagsByNewsId(Long newsId) throws ServiceException {
		Collection<Tag> tags = null;
		try {
			tags = tagDAO.selectAllTagsByNewsId(newsId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get tags by news id. ", ex);
		}
		return tags;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.TagService#getTagsByEachNewsId(java.util.Collection)
	 */
	@Override
	public Map<Long, Collection<Tag>> getTagsByEachNewsId(Collection<Long> newsIds) throws ServiceException {
		Map<Long, Collection<Tag>> mapOfNewsIdsAndTagLists = null;
		try {
			mapOfNewsIdsAndTagLists = tagDAO.selectTagsByEachNewsId(newsIds);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all tags for each news. ", ex);
		}
		return mapOfNewsIdsAndTagLists;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.TagService#deleteTagFromNews(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteTagFromNews(Long newsId, Long tagId) throws ServiceException {
		try {
			tagDAO.deleteTagFromNews(newsId, tagId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete tag from news. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.TagService#deleteAllTagsFromNews(java.lang.Long)
	 */
	@Override
	public void deleteAllTagsFromNews(Long newsId) throws ServiceException {
		try {
			tagDAO.deleteAllTagsFromNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete all tags from news. ", ex);
		}
	}
}
