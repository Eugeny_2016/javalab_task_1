package by.epam.newsmng.service;

import java.util.Collection;

import by.epam.newsmng.domain.AssembledNews;
import by.epam.newsmng.domain.SearchCriteria;

// TODO: Auto-generated Javadoc
/**
 * The Interface AssembledNewsService.
 */
public interface AssembledNewsService extends IService<AssembledNews> {
	
	/**
	 * Gets the by title.
	 *
	 * @param newsTitle the news title
	 * @return the by title
	 * @throws ServiceException the service exception
	 */
	public AssembledNews getByTitle(String newsTitle) throws ServiceException;
	
	/**
	 * Gets the next n_ news.
	 *
	 * @param maxValue the max value
	 * @param minValue the min value
	 * @return the next n_ news
	 * @throws ServiceException the service exception
	 */
	public Collection<AssembledNews> getNextN_News(long maxValue, long minValue) throws ServiceException;
	
	/**
	 * Gets the news by search criteria.
	 *
	 * @param searchCriteria the search criteria
	 * @return the news by search criteria
	 * @throws ServiceException the service exception
	 */
	public Collection<AssembledNews> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Gets the n most commented news id.
	 *
	 * @param amountOfNewsId the amount of news id
	 * @return the n most commented news id
	 * @throws ServiceException the service exception
	 */
	public Collection<AssembledNews> getNMostCommentedNewsId(int amountOfNewsId) throws ServiceException;
	
	/**
	 * Count all news.
	 *
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public long countAllNews() throws ServiceException;
	
	/**
	 * Count news by search criteria.
	 *
	 * @param serachCriteria the serach criteria
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public long countNewsBySearchCriteria(SearchCriteria serachCriteria) throws ServiceException;
}
