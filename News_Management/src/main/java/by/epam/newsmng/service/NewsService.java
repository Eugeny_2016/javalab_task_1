package by.epam.newsmng.service;

import java.util.Collection;

import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.SearchCriteria;

// TODO: Auto-generated Javadoc
/**
 * The Interface NewsService.
 */
public interface NewsService extends IService<News> {
	
	/**
	 * Gets the by title.
	 *
	 * @param newsTitle the news title
	 * @return the by title
	 * @throws ServiceException the service exception
	 */
	public News getByTitle(String newsTitle) throws ServiceException;
	
	/**
	 * Gets the several news.
	 *
	 * @param newsIds the news ids
	 * @return the several news
	 * @throws ServiceException the service exception
	 */
	public Collection<News> getSeveralNews(Collection<Long> newsIds) throws ServiceException;
	
	/**
	 * Gets the next n_ news.
	 *
	 * @param maxValue the max value
	 * @param minValue the min value
	 * @return the next n_ news
	 * @throws ServiceException the service exception
	 */
	public Collection<News> getNextN_News(long maxValue, long minValue) throws ServiceException;
	
	/**
	 * Gets the news by search criteria.
	 *
	 * @param searchCriteria the search criteria
	 * @return the news by search criteria
	 * @throws ServiceException the service exception
	 */
	public Collection<News> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Delete by title.
	 *
	 * @param newsTitle the news title
	 * @throws ServiceException the service exception
	 */
	public void deleteByTitle(String newsTitle) throws ServiceException;
	
	/**
	 * Count all news.
	 *
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public long countAllNews() throws ServiceException;
	
	/**
	 * Count news by search criteria.
	 *
	 * @param serachCriteria the serach criteria
	 * @return the long
	 * @throws ServiceException the service exception
	 */
	public long countNewsBySearchCriteria(SearchCriteria serachCriteria) throws ServiceException;
}
