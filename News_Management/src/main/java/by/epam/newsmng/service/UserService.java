package by.epam.newsmng.service;

import by.epam.newsmng.domain.User;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserService.
 */
public interface UserService extends IService<User> {
	
	/**
	 * Gets the by login.
	 *
	 * @param login the login
	 * @return the by login
	 * @throws ServiceException the service exception
	 */
	public User getByLogin(String login) throws ServiceException;
	
}
