package by.epam.newsmng.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmng.domain.AssembledNews;
import by.epam.newsmng.domain.Author;
import by.epam.newsmng.domain.Comment;
import by.epam.newsmng.domain.News;
import by.epam.newsmng.domain.SearchCriteria;
import by.epam.newsmng.domain.Tag;
import by.epam.newsmng.service.AssembledNewsService;
import by.epam.newsmng.service.AuthorService;
import by.epam.newsmng.service.CommentService;
import by.epam.newsmng.service.NewsService;
import by.epam.newsmng.service.ServiceException;
import by.epam.newsmng.service.TagService;

// TODO: Auto-generated Javadoc
/**
 * The Class AssembledNewsServiceGroup.
 */
@Service
public class AssembledNewsServiceGroup implements AssembledNewsService {
	
	/** The Constant COUNTING_QUERY. */
	public static final String COUNTING_QUERY = "SELECT COUNT(*) FROM ";
	
	/** The Constant DROPPING_SEQUENCE_QUERY. */
	public static final String DROPPING_SEQUENCE_QUERY = "DROP SEQUENCE ";
	
	/** The Constant CREATING_SEQUENCE_QUERY_PART1. */
	public static final String CREATING_SEQUENCE_QUERY_PART1 = "CREATE SEQUENCE ";
	
	/** The Constant CREATING_SEQUENCE_QUERY_PART2. */
	public static final String CREATING_SEQUENCE_QUERY_PART2 = " START WITH ";
	
	/** The Constant NEWS_TABLE. */
	public static final String NEWS_TABLE = "NEWS";
	
	/** The Constant AUTHOR_TABLE. */
	public static final String AUTHOR_TABLE = "AUTHOR";
	
	/** The Constant TAG_TABLE. */
	public static final String TAG_TABLE = "TAG";
	
	/** The Constant COMMENT_TABLE. */
	public static final String COMMENT_TABLE = "COMMENT";
	
	/** The news service. */
	@Autowired
	private NewsService newsService;
	
	/** The author service. */
	@Autowired
	private AuthorService authorService;
	
	/** The tag service. */
	@Autowired
	private TagService tagService;
	
	/** The comment service. */
	@Autowired
	private CommentService commentService;
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#add(java.lang.Object)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = ServiceException.class)
	public Long add(AssembledNews assembledNews) throws ServiceException {
		Long newsId = null;
		Collection<String> setOfAuthorNames = new ArrayList<String>();
		Collection<String> setOfTagNames = new ArrayList<String>();
		Collection<Long> setOfAuthorIds = new ArrayList<Long>();
		Collection<Long> setOfTagIds = new ArrayList<Long>();
		Collection<Author> recievedAuthors = new ArrayList<Author>();
		Collection<Tag> recievedTags = new ArrayList<Tag>();
		Collection<Author> existingAuthors = null;
		Collection<Tag> existingTags = null;	
		
		// adding news
		newsId = newsService.add(assembledNews.getNews());
		
		// finding existing authors, creating only new ones, linking all of them to news
		for(Author author : assembledNews.getAuthors()) {
			setOfAuthorNames.add(author.getAuthorName());
			setOfAuthorIds.add(author.getAuthorId());
		}
		existingAuthors = authorService.getExistingAuthorsByNames(setOfAuthorNames);
		recievedAuthors.addAll(assembledNews.getAuthors());
		recievedAuthors.removeAll(existingAuthors);
		if (!recievedAuthors.isEmpty()) {
			authorService.addAllAuthors(recievedAuthors);
		}
		authorService.addAuthorsToNews(assembledNews.getNews().getNewsId(), setOfAuthorIds);
		
		// finding existing tags, creating only new ones, linking all of them to news
		for(Tag tag : assembledNews.getTags()) {
			setOfTagNames.add(tag.getTagName());
			setOfTagIds.add(tag.getTagId());
		}
		existingTags = tagService.getExistingTagsByNames(setOfTagNames);
		recievedTags.addAll(assembledNews.getTags());
		recievedTags.removeAll(existingTags);
		if (!recievedTags.isEmpty()) {
			tagService.addAllTags(recievedTags);
		}		
		tagService.addTagsToNews(assembledNews.getNews().getNewsId(), setOfTagIds);
		
		// adding comments and linking them to news
		Collection<Comment> comments = assembledNews.getComments();
		for (Comment comment : comments) {
			commentService.add(comment);
		}
		return newsId;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getById(java.lang.Long)
	 */
	@Override
	public AssembledNews getById(Long newsId) throws ServiceException {
		AssembledNews assembledNews = new AssembledNews();
		News news = newsService.getById(newsId);
		Collection<Author> authors = authorService.getAuthorsByNewsId(newsId);
		Collection<Tag> tags = tagService.getTagsByNewsId(newsId);
		Collection<Comment> comments = commentService.getCommentsByNewsId(newsId);
		
		assembledNews.setNews(news);
		assembledNews.setAuthors(authors);
		assembledNews.setTags(tags);
		assembledNews.setComments(comments);		
		
		return assembledNews;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getAll()
	 */
	@Override
	public Collection<AssembledNews> getAll() throws ServiceException {
		Collection<Long> listOfNewsIds = new ArrayList<Long>();		
		List<News> listOfNews = (List<News>) newsService.getAll();
		
		for (News news : listOfNews) {
			listOfNewsIds.add(news.getNewsId());
		}
		return getAssembledNews(listOfNewsIds, listOfNews);
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(AssembledNews obj) throws ServiceException {
		throw new UnsupportedOperationException("Assembled news are updated partially only");
		
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#deleteById(java.lang.Long)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, rollbackFor = ServiceException.class)
	public void deleteById(Long newsId) throws ServiceException {
		commentService.deleteCommentsFromNews(newsId);
		authorService.deleteAllAuthorsFromNews(newsId);
		tagService.deleteAllTagsFromNews(newsId);
		newsService.deleteById(newsId);		
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AssembledNewsService#getByTitle(java.lang.String)
	 */
	@Override
	public AssembledNews getByTitle(String newsTitle) throws ServiceException {
		AssembledNews assembledNews = new AssembledNews();
		News news = newsService.getByTitle(newsTitle);
		Collection<Author> authors = authorService.getAuthorsByNewsId(news.getNewsId());
		Collection<Tag> tags = tagService.getTagsByNewsId(news.getNewsId());
		Collection<Comment> comments = commentService.getCommentsByNewsId(news.getNewsId());
		
		assembledNews.setNews(news);
		assembledNews.setAuthors(authors);
		assembledNews.setTags(tags);
		assembledNews.setComments(comments);	
		return assembledNews;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AssembledNewsService#getNextN_News(long, long)
	 */
	@Override
	public Collection<AssembledNews> getNextN_News(long maxValue, long minValue) throws ServiceException {
		Collection<Long> listOfNewsIds = new ArrayList<Long>();
		List<News> listOfNews = (List<News>) newsService.getNextN_News(maxValue, minValue);
		
		for (News news : listOfNews) {
			listOfNewsIds.add(news.getNewsId());
		}
		return getAssembledNews(listOfNewsIds, listOfNews);
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AssembledNewsService#getNewsBySearchCriteria(by.epam.newsmng.domain.SearchCriteria)
	 */
	@Override
	public Collection<AssembledNews> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		Collection<Long> listOfNewsIds = new ArrayList<Long>();
		List<News> listOfNews = (List<News>) newsService.getNewsBySearchCriteria(searchCriteria);
		
		for (News news : listOfNews) {
			listOfNewsIds.add(news.getNewsId());
		}
		return getAssembledNews(listOfNewsIds, listOfNews);
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AssembledNewsService#getNMostCommentedNewsId(int)
	 */
	@Override
	public Collection<AssembledNews> getNMostCommentedNewsId(int amountOfNewsId) throws ServiceException {
		Collection<Long> listOfNewsIds = commentService.getNMostCommentedNewsId(amountOfNewsId);
		List<News> listOfNews = (List<News>) newsService.getSeveralNews(listOfNewsIds);
		return getAssembledNews(listOfNewsIds, listOfNews);
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AssembledNewsService#countAllNews()
	 */
	@Override
	public long countAllNews() throws ServiceException {
		return newsService.countAllNews();
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AssembledNewsService#countNewsBySearchCriteria(by.epam.newsmng.domain.SearchCriteria)
	 */
	@Override
	public long countNewsBySearchCriteria(SearchCriteria serachCriteria) throws ServiceException {
		return newsService.countNewsBySearchCriteria(serachCriteria);
	}
	
	/**
	 * Gets the assembled news.
	 *
	 * @param listOfNewsIds the list of news ids
	 * @param listOfNews the list of news
	 * @return the assembled news
	 * @throws ServiceException the service exception
	 */
	private Collection<AssembledNews> getAssembledNews(Collection<Long> listOfNewsIds, List<News> listOfNews) throws ServiceException {
		Collection<AssembledNews> listOfAssembledNews = new ArrayList<AssembledNews>();
				
		Map<Long, Collection<Author>> authors = authorService.getAuthorsByEachNewsId(listOfNewsIds);
		Map<Long, Collection<Tag>> tags = tagService.getTagsByEachNewsId(listOfNewsIds);
		Map<Long, Collection<Comment>> comments = commentService.getCommentsByEachNewsId(listOfNewsIds);
		
		for (int i = 0; i < listOfNewsIds.size(); i++) {
			AssembledNews assembledNews = new AssembledNews();
			assembledNews.setNews(listOfNews.get(i));
			assembledNews.setAuthors(authors.get(i));
			assembledNews.setTags(tags.get(i));
			assembledNews.setComments(comments.get(i));
		}
		return listOfAssembledNews;
	}
	
}
