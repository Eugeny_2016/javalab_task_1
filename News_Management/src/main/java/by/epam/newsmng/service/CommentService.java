package by.epam.newsmng.service;

import java.util.Collection;
import java.util.Map;

import by.epam.newsmng.domain.Comment;

// TODO: Auto-generated Javadoc
/**
 * The Interface CommentService.
 */
public interface CommentService extends IService<Comment> {
	
	/**
	 * Gets the comments by news id.
	 *
	 * @param newsId the news id
	 * @return the comments by news id
	 * @throws ServiceException the service exception
	 */
	public Collection<Comment> getCommentsByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Censure comment.
	 *
	 * @param commentId the comment id
	 * @param messageWithReason the message with reason
	 * @throws ServiceException the service exception
	 */
	public void censureComment(Long commentId, String messageWithReason) throws ServiceException;
	
	/**
	 * Gets the comments by each news id.
	 *
	 * @param newsIds the news ids
	 * @return the comments by each news id
	 * @throws ServiceException the service exception
	 */
	public Map<Long, Collection<Comment>> getCommentsByEachNewsId(Collection<Long> newsIds) throws ServiceException;
	
	/**
	 * Delete comment from news.
	 *
	 * @param newsId the news id
	 * @param commentId the comment id
	 * @throws ServiceException the service exception
	 */
	public void deleteCommentFromNews(Long newsId, Long commentId) throws ServiceException;
	
	/**
	 * Delete comments from news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	public void deleteCommentsFromNews(Long newsId) throws ServiceException;
	
	/**
	 * Gets the n most commented news id.
	 *
	 * @param amountOfNewsId the amount of news id
	 * @return the n most commented news id
	 * @throws ServiceException the service exception
	 */
	public Collection<Long> getNMostCommentedNewsId(int amountOfNewsId) throws ServiceException;

}
