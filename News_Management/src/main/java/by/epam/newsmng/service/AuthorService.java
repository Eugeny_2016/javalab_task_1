package by.epam.newsmng.service;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Map;

import by.epam.newsmng.domain.Author;

// TODO: Auto-generated Javadoc
/**
 * The Interface AuthorService.
 */
public interface AuthorService extends IService<Author> {
	
	/**
	 * Make author expired by id.
	 *
	 * @param authorId the author id
	 * @param expiryDate the expiry date
	 * @throws ServiceException the service exception
	 */
	public void makeAuthorExpiredById(Long authorId, Timestamp expiryDate) throws ServiceException;
	
	/**
	 * Gets the by name.
	 *
	 * @param authorName the author name
	 * @return the by name
	 * @throws ServiceException the service exception
	 */
	public Author getByName(String authorName) throws ServiceException;
	
	/**
	 * Gets the existing authors by names.
	 *
	 * @param authorNames the author names
	 * @return the existing authors by names
	 * @throws ServiceException the service exception
	 */
	public Collection<Author> getExistingAuthorsByNames(Collection<String> authorNames) throws ServiceException;
	
	/**
	 * Adds the all authors.
	 *
	 * @param authors the authors
	 * @throws ServiceException the service exception
	 */
	public void addAllAuthors(Collection<Author> authors) throws ServiceException;
	
	/**
	 * Adds the authors to news.
	 *
	 * @param newsId the news id
	 * @param authorIds the author ids
	 * @throws ServiceException the service exception
	 */
	public void addAuthorsToNews(Long newsId, Collection<Long> authorIds) throws ServiceException;
	
	/**
	 * Gets the authors by news id.
	 *
	 * @param newsId the news id
	 * @return the authors by news id
	 * @throws ServiceException the service exception
	 */
	public Collection<Author> getAuthorsByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Gets the authors by each news id.
	 *
	 * @param newsIds the news ids
	 * @return the authors by each news id
	 * @throws ServiceException the service exception
	 */
	public Map<Long, Collection<Author>> getAuthorsByEachNewsId(Collection<Long> newsIds) throws ServiceException;
	
	/**
	 * Delete author from news.
	 *
	 * @param newsId the news id
	 * @param authorId the author id
	 * @throws ServiceException the service exception
	 */
	public void deleteAuthorFromNews(Long newsId, Long authorId) throws ServiceException;
	
	/**
	 * Delete all authors from news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	public void deleteAllAuthorsFromNews(Long newsId) throws ServiceException;

}
