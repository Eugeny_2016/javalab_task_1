package by.epam.newsmng.service;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceException.
 */
public class ServiceException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new service exception.
	 *
	 * @param msg the msg
	 */
	public ServiceException(String msg) {
		super(msg);
	}
	
	/**
	 * Instantiates a new service exception.
	 *
	 * @param msg the msg
	 * @param ex the ex
	 */
	public ServiceException(String msg, Exception ex) {
		super(msg, ex);
	}

}
