package by.epam.newsmng.service.impl;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newsmng.dao.AuthorDAO;
import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.domain.Author;
import by.epam.newsmng.service.AuthorService;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthorServiceGroup.
 */
@Service
public class AuthorServiceGroup implements AuthorService {
	
	/** The author dao. */
	@Autowired
	private AuthorDAO authorDAO;
		
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#add(java.lang.Object)
	 */
	@Override
	public Long add(Author author) throws ServiceException {
		Long authorId = null;
		try {
			authorId = authorDAO.insert(author);
		} catch (DAOException ex) {
			throw new ServiceException("Can't add a new author. ", ex);
		}
		return authorId;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getById(java.lang.Long)
	 */
	@Override
	public Author getById(Long authorId) throws ServiceException {
		Author author = null;
		try {
			author = authorDAO.selectById(authorId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get an author by id. ", ex);
		}
		return author;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getAll()
	 */
	@Override
	public Collection<Author> getAll() throws ServiceException {
		Collection<Author> listOfAuthors = null;
		try {
			listOfAuthors = authorDAO.selectAll();
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all authors. ", ex);
		}
		return listOfAuthors;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(Author author) throws ServiceException {
		try {
			authorDAO.update(author);
		} catch (DAOException ex) {
			throw new ServiceException("Can't update an author. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long authorId) throws ServiceException {
		try {
			authorDAO.deleteById(authorId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete author by id. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#makeAuthorExpiredById(java.lang.Long, java.sql.Timestamp)
	 */
	@Override
	public void makeAuthorExpiredById(Long authorId, Timestamp expiryDate) throws ServiceException {
		try {
			authorDAO.updateAuthorExpiredById(authorId, expiryDate);
		} catch (DAOException ex) {
			throw new ServiceException("Can't make author expired. ", ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#getByName(java.lang.String)
	 */
	@Override
	public Author getByName(String authorName) throws ServiceException {
		Author author = null;
		try {
			author = authorDAO.selectByName(authorName);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get an author by name. ", ex);
		}
		return author;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#getExistingAuthorsByNames(java.util.Collection)
	 */
	@Override
	public Collection<Author> getExistingAuthorsByNames(Collection<String> authorNames) throws ServiceException {
		Collection<Author> authors = null;
		try {
			authors = authorDAO.selectExistingAuthorsByNames(authorNames);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get the existing authors. ", ex);
		}
		return authors;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#addAllAuthors(java.util.Collection)
	 */
	@Override
	public void addAllAuthors(Collection<Author> authors) throws ServiceException {
		try {
			authorDAO.insertAllAuthors(authors);
		} catch (DAOException ex) {
			throw new ServiceException("Can't add authors. ", ex);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#addAuthorsToNews(java.lang.Long, java.util.Collection)
	 */
	@Override
	public void addAuthorsToNews(Long newsId, Collection<Long> authorIds) throws ServiceException {
		try {
			authorDAO.insertAuthorsToNews(newsId, authorIds);
		} catch (DAOException ex) {
			throw new ServiceException("Can't attach authors to news. ", ex);
		}	
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#getAuthorsByNewsId(java.lang.Long)
	 */
	@Override
	public Collection<Author> getAuthorsByNewsId(Long newsId) throws ServiceException {
		Collection<Author> listOfAuthors = null;
		try {
			listOfAuthors = authorDAO.selectAuthorsByNewsId(newsId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all authors by news id. ", ex);
		}
		return listOfAuthors;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#getAuthorsByEachNewsId(java.util.Collection)
	 */
	@Override
	public Map<Long, Collection<Author>> getAuthorsByEachNewsId(Collection<Long> newsIds) throws ServiceException {
		Map<Long, Collection<Author>> mapOfNewsIdsAndAuthorlists = null;
		try {
			mapOfNewsIdsAndAuthorlists = authorDAO.selectAuthorsByEachNewsId(newsIds);
		} catch (DAOException ex) {
			throw new ServiceException("Can't get all authors for each news. ", ex);
		}
		return mapOfNewsIdsAndAuthorlists;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#deleteAuthorFromNews(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteAuthorFromNews(Long newsId, Long authorId) throws ServiceException {
		try {
			authorDAO.deleteAuthorFromNews(newsId, authorId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete author from news. ", ex);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.AuthorService#deleteAllAuthorsFromNews(java.lang.Long)
	 */
	@Override
	public void deleteAllAuthorsFromNews(Long newsId) throws ServiceException {
		try {
			authorDAO.deleteAllAuthorsFromNews(newsId);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete all authors from news. ", ex);
		}
	}	
}
