package by.epam.newsmng.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import by.epam.newsmng.dao.DAOException;
import by.epam.newsmng.dao.RoleDAO;
import by.epam.newsmng.domain.Role;
import by.epam.newsmng.service.RoleService;
import by.epam.newsmng.service.ServiceException;

// TODO: Auto-generated Javadoc
/**
 * The Class RoleServiceGroup.
 */
public class RoleServiceGroup implements RoleService {
	
	/** The role dao. */
	@Autowired
	private RoleDAO roleDAO;
	
	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#add(java.lang.Object)
	 */
	@Override
	public Long add(Role obj) throws ServiceException {
		Long roleId;
		try {
			roleId = roleDAO.insert(obj);
		} catch(DAOException ex) {
			throw new ServiceException("Can't add role. " + ex);
		}
		return roleId;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getById(java.lang.Long)
	 */
	@Override
	public Role getById(Long id) throws ServiceException {
		Role role;
		try {
			role = roleDAO.selectById(id);
		} catch(DAOException ex) {
			throw new ServiceException("Can't get role by id. " + ex);
		}
		return role;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#getAll()
	 */
	@Override
	public Collection<Role> getAll() throws ServiceException {
		Collection<Role> roles;
		try {
			roles = roleDAO.selectAll();
		} catch(DAOException ex) {
			throw new ServiceException("Can't get all roles. " + ex);
		}
		return roles;
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#update(java.lang.Object)
	 */
	@Override
	public void update(Role obj) throws ServiceException {
		try {
			roleDAO.update(obj);
		} catch (DAOException ex) {
			throw new ServiceException("Can't update role name. " + ex);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.IService#deleteById(java.lang.Long)
	 */
	@Override
	public void deleteById(Long id) throws ServiceException {
		try {
			roleDAO.deleteById(id);
		} catch (DAOException ex) {
			throw new ServiceException("Can't delete role by id. " + ex);
		}
	}

	/* (non-Javadoc)
	 * @see by.epam.newsmng.service.RoleService#getByName(java.lang.String)
	 */
	@Override
	public Role getByName(String roleName) throws ServiceException {
		Role role;
		try {
			role = roleDAO.selectByName(roleName);
		} catch(DAOException ex) {
			throw new ServiceException("Can't get role by name. " + ex);
		}
		return role;
	}

}
