package by.epam.newsmng.service;

import java.util.Collection;
import java.util.Map;

import by.epam.newsmng.domain.Tag;

// TODO: Auto-generated Javadoc
/**
 * The Interface TagService.
 */
public interface TagService extends IService<Tag> {
	
	/**
	 * Gets the existing tags by names.
	 *
	 * @param tagNames the tag names
	 * @return the existing tags by names
	 * @throws ServiceException the service exception
	 */
	public Collection<Tag> getExistingTagsByNames(Collection<String> tagNames) throws ServiceException;
	
	/**
	 * Adds the all tags.
	 *
	 * @param tags the tags
	 * @throws ServiceException the service exception
	 */
	public void addAllTags(Collection<Tag> tags) throws ServiceException;
	
	/**
	 * Adds the tags to news.
	 *
	 * @param newsId the news id
	 * @param tagIds the tag ids
	 * @throws ServiceException the service exception
	 */
	public void addTagsToNews(Long newsId, Collection<Long> tagIds) throws ServiceException;
	
	/**
	 * Gets the tags by news id.
	 *
	 * @param newsId the news id
	 * @return the tags by news id
	 * @throws ServiceException the service exception
	 */
	public Collection<Tag> getTagsByNewsId(Long newsId) throws ServiceException;
		
	/**
	 * Gets the tags by each news id.
	 *
	 * @param newsIds the news ids
	 * @return the tags by each news id
	 * @throws ServiceException the service exception
	 */
	public Map<Long, Collection<Tag>> getTagsByEachNewsId(Collection<Long> newsIds) throws ServiceException;
	
	/**
	 * Delete tag from news.
	 *
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws ServiceException the service exception
	 */
	public void deleteTagFromNews(Long newsId, Long tagId) throws ServiceException;
	
	/**
	 * Delete all tags from news.
	 *
	 * @param newsId the news id
	 * @throws ServiceException the service exception
	 */
	public void deleteAllTagsFromNews(Long newsId) throws ServiceException;
}
